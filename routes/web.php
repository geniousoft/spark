<?php

use App\Http\Controllers\TwitterController;
use Illuminate\Support\Facades\Route;
use Spatie\Health\Http\Controllers\HealthCheckResultsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/auth/redirect',[TwitterController::class,'redirect'])->name('twitter.login');
 
Route::get('/auth/callback',[TwitterController::class,'callback']);
Route::get('/', HealthCheckResultsController::class);
Route::post('/logout',[TwitterController::class,'logout'])->name('twitter.logout');
Route::post('/profile',[TwitterController::class,'save'])->name('twitter.update');
Route::get('/test', [TwitterController::class,'test'])->name('test');





/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('admin-users')->name('admin-users/')->group(static function() {
            Route::get('/',                                             'AdminUsersController@index')->name('index');
            Route::get('/create',                                       'AdminUsersController@create')->name('create');
            Route::post('/',                                            'AdminUsersController@store')->name('store');
            Route::get('/{adminUser}/impersonal-login',                 'AdminUsersController@impersonalLogin')->name('impersonal-login');
            Route::get('/{adminUser}/edit',                             'AdminUsersController@edit')->name('edit');
            Route::post('/{adminUser}',                                 'AdminUsersController@update')->name('update');
            Route::delete('/{adminUser}',                               'AdminUsersController@destroy')->name('destroy');
            Route::get('/{adminUser}/resend-activation',                'AdminUsersController@resendActivationEmail')->name('resendActivationEmail');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::get('/',                                      'DashboardController@index')->name('dashboard');
        Route::get('/profile',                                      'ProfileController@editProfile')->name('edit-profile');
        Route::post('/profile',                                     'ProfileController@updateProfile')->name('update-profile');
        Route::get('/password',                                     'ProfileController@editPassword')->name('edit-password');
        Route::post('/password',                                    'ProfileController@updatePassword')->name('update-password');
    });
});




/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('roadmaps')->name('roadmaps/')->group(static function() {
            Route::get('/',                                             'RoadmapController@index')->name('index');
            Route::get('/create',                                       'RoadmapController@create')->name('create');
            Route::post('/',                                            'RoadmapController@store')->name('store');
            Route::get('/{roadmap}/edit',                               'RoadmapController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'RoadmapController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{roadmap}',                                   'RoadmapController@update')->name('update');
            Route::delete('/{roadmap}',                                 'RoadmapController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('profiles')->name('profiles/')->group(static function() {
            Route::get('/',                                             'ProfilesController@index')->name('index');
            Route::get('/create',                                       'ProfilesController@create')->name('create');
            Route::post('/',                                            'ProfilesController@store')->name('store');
            Route::get('/{profile}/edit',                               'ProfilesController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'ProfilesController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{profile}',                                   'ProfilesController@update')->name('update');
            Route::delete('/{profile}',                                 'ProfilesController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('projects')->name('projects/')->group(static function() {
            Route::get('/',                                             'ProjectsController@index')->name('index');
            Route::get('/create',                                       'ProjectsController@create')->name('create');
            Route::post('/',                                            'ProjectsController@store')->name('store');
            Route::get('/{project}/edit',                               'ProjectsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'ProjectsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{project}',                                   'ProjectsController@update')->name('update');
            Route::delete('/{project}',                                 'ProjectsController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('campaigns')->name('campaigns/')->group(static function() {
            Route::get('/',                                             'CampaignsController@index')->name('index');
            Route::get('/create',                                       'CampaignsController@create')->name('create');
            Route::post('/',                                            'CampaignsController@store')->name('store');
            Route::get('/{campaign}/edit',                              'CampaignsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'CampaignsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{campaign}',                                  'CampaignsController@update')->name('update');
            Route::delete('/{campaign}',                                'CampaignsController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('campaign-requests')->name('campaign-requests/')->group(static function() {
            Route::get('/',                                             'CampaignRequestsController@index')->name('index');
            Route::get('/create',                                       'CampaignRequestsController@create')->name('create');
            Route::post('/',                                            'CampaignRequestsController@store')->name('store');
            Route::get('/{campaignRequest}/edit',                       'CampaignRequestsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'CampaignRequestsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{campaignRequest}',                           'CampaignRequestsController@update')->name('update');
            Route::delete('/{campaignRequest}',                         'CampaignRequestsController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('profile-stats')->name('profile-stats/')->group(static function() {
            Route::get('/',                                             'ProfileStatsController@index')->name('index');
            Route::get('/create',                                       'ProfileStatsController@create')->name('create');
            Route::post('/',                                            'ProfileStatsController@store')->name('store');
            Route::get('/{profileStat}/edit',                           'ProfileStatsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'ProfileStatsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{profileStat}',                               'ProfileStatsController@update')->name('update');
            Route::delete('/{profileStat}',                             'ProfileStatsController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('campaign-stats')->name('campaign-stats/')->group(static function() {
            Route::get('/',                                             'CampaignStatsController@index')->name('index');
            Route::get('/create',                                       'CampaignStatsController@create')->name('create');
            Route::post('/',                                            'CampaignStatsController@store')->name('store');
            Route::get('/{campaignStat}/edit',                          'CampaignStatsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'CampaignStatsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{campaignStat}',                              'CampaignStatsController@update')->name('update');
            Route::delete('/{campaignStat}',                            'CampaignStatsController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('n-f-t-collections')->name('n-f-t-collections/')->group(static function() {
            Route::get('/',                                             'NFTCollectionsController@index')->name('index');
            Route::get('/create',                                       'NFTCollectionsController@create')->name('create');
            Route::post('/',                                            'NFTCollectionsController@store')->name('store');
            Route::get('/{nFTCollection}/edit',                         'NFTCollectionsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'NFTCollectionsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{nFTCollection}',                             'NFTCollectionsController@update')->name('update');
            Route::delete('/{nFTCollection}',                           'NFTCollectionsController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('nft-collections')->name('nft-collections/')->group(static function() {
            Route::get('/',                                             'NftCollectionsController@index')->name('index');
            Route::get('/create',                                       'NftCollectionsController@create')->name('create');
            Route::post('/',                                            'NftCollectionsController@store')->name('store');
            Route::get('/{nftCollection}/edit',                         'NftCollectionsController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'NftCollectionsController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{nftCollection}',                             'NftCollectionsController@update')->name('update');
            Route::delete('/{nftCollection}',                           'NftCollectionsController@destroy')->name('destroy');
        });
    });
});

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
    Route::prefix('admin')->namespace('App\Http\Controllers\Admin')->name('admin/')->group(static function() {
        Route::prefix('twit-links')->name('twit-links/')->group(static function() {
            Route::get('/',                                             'TwitLinksController@index')->name('index');
            Route::get('/create',                                       'TwitLinksController@create')->name('create');
            Route::post('/',                                            'TwitLinksController@store')->name('store');
            Route::get('/{twitLink}/edit',                              'TwitLinksController@edit')->name('edit');
            Route::post('/bulk-destroy',                                'TwitLinksController@bulkDestroy')->name('bulk-destroy');
            Route::post('/{twitLink}',                                  'TwitLinksController@update')->name('update');
            Route::delete('/{twitLink}',                                'TwitLinksController@destroy')->name('destroy');
        });
    });
});