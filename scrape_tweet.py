import requests, json

TWEET_ID = "1632850531755368455"
TWEER_URL = f'https://api.twitter.com/graphql/zXaXQgfyR4GxE21uwYQSyA/TweetDetail?variables=%7B%22focalTweetId%22%3A%22{TWEET_ID}%22%2C%22with_rux_injections%22%3Afalse%2C%22includePromotedContent%22%3Atrue%2C%22withCommunity%22%3Atrue%2C%22withQuickPromoteEligibilityTweetFields%22%3Atrue%2C%22withBirdwatchNotes%22%3Afalse%2C%22withSuperFollowsUserFields%22%3Atrue%2C%22withDownvotePerspective%22%3Afalse%2C%22withReactionsMetadata%22%3Afalse%2C%22withReactionsPerspective%22%3Afalse%2C%22withSuperFollowsTweetFields%22%3Atrue%2C%22withVoice%22%3Atrue%2C%22withV2Timeline%22%3Atrue%7D&features=%7B%22responsive_web_twitter_blue_verified_badge_is_enabled%22%3Atrue%2C%22responsive_web_graphql_exclude_directive_enabled%22%3Atrue%2C%22verified_phone_label_enabled%22%3Afalse%2C%22responsive_web_graphql_timeline_navigation_enabled%22%3Atrue%2C%22responsive_web_graphql_skip_user_profile_image_extensions_enabled%22%3Afalse%2C%22tweetypie_unmention_optimization_enabled%22%3Atrue%2C%22vibe_api_enabled%22%3Atrue%2C%22responsive_web_edit_tweet_api_enabled%22%3Atrue%2C%22graphql_is_translatable_rweb_tweet_is_translatable_enabled%22%3Atrue%2C%22view_counts_everywhere_api_enabled%22%3Atrue%2C%22longform_notetweets_consumption_enabled%22%3Atrue%2C%22tweet_awards_web_tipping_enabled%22%3Afalse%2C%22freedom_of_speech_not_reach_fetch_enabled%22%3Afalse%2C%22standardized_nudges_misinfo%22%3Atrue%2C%22tweet_with_visibility_results_prefer_gql_limited_actions_policy_enabled%22%3Afalse%2C%22interactive_text_enabled%22%3Atrue%2C%22responsive_web_text_conversations_enabled%22%3Afalse%2C%22longform_notetweets_richtext_consumption_enabled%22%3Afalse%2C%22responsive_web_enhance_cards_enabled%22%3Afalse%7D'
headers = {'authority': 'api.twitter.com', 
   'accept': '*/*', 
   'accept-language': 'en-US,en;q=0.5', 
   'authorization': 'Bearer AAAAAAAAAAAAAAAAAAAAANRILgAAAAAAnNwIzUejRCOuH5E6I8xnZz4puTs%3D1Zv7ttfk8LF81IUq16cHjhLTvJu4FA33AGWWjCpTnA', 
   'cache-control': 'no-cache', 
   'content-type': 'application/json', 
   'cookie': 'guest_id_ads=v1%3A167847036644225648; guest_id_marketing=v1%3A167847036644225648; guest_id=v1%3A167847036644225648; ct0=909b792e8735d17fb1c8377722a7b30d; gt=1634249439375138820; personalization_id="v1_sLV0i4OCz33f9jFBOAA0/A=="', 
   'dnt': '1', 
   'origin': 'https://twitter.com', 
   'pragma': 'no-cache', 
   'referer': 'https://twitter.com/', 
   'sec-fetch-dest': 'empty', 
   'sec-fetch-mode': 'cors', 
   'sec-fetch-site': 'same-site', 
   'sec-gpc': '1', 
   'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36',
   'x-csrf-token': '909b792e8735d17fb1c8377722a7b30d', 
   'x-guest-token': '1634249439375138820', 
   'x-twitter-active-user': 'yes', 
   'x-twitter-client-language': 'en' 
   }
  


page = requests.get(TWEER_URL, headers=headers)

data = json.loads(page.text)
print(data.keys)
#text = data["data"]["threaded_conversation_with_injections_v2"]["instructions"][0]["entries"][0]["content"]["itemContent"]["tweet_results"]["result"]["tweet"]["note_tweet"]["note_tweet_results"]["result"]["text"]
