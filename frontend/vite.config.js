import { fileURLToPath, URL } from 'node:url';
import { VitePWA } from 'vite-plugin-pwa'

import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';

import svgLoader from 'vite-svg-loader';
import { NodeGlobalsPolyfillPlugin } from '@esbuild-plugins/node-globals-polyfill'
import nodePolyfills from 'rollup-plugin-node-polyfills';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), svgLoader(),
  VitePWA({
    injectRegister: "auto",
    manifest: {
      name: "Spark",
      short_name: "Spark",
      theme_color: "#000000",
      background_color: "#000000",
      categories: ["Spark", "Influencer", "web3"],
      description: "Spark is next influencer marketing",
      icons: [
        {
          "src": "https://sparkonline.xyz/assets/images/logos_s.png",
          "sizes": "48x48",
          "type": "image/png"
        }, {
          "src": "https://sparkonline.xyz/assets/images/logos_s.png",
          "sizes": "72x72",
          "type": "image/png"
        }, {
          "src": "https://sparkonline.xyz/assets/images/logos_s.png",
          "sizes": "96x96",
          "type": "image/png"
        }, {
          "src": "https://sparkonline.xyz/assets/images/logos_s.png",
          "sizes": "144x144",
          "type": "image/png"
        }, {
          "src": "https://sparkonline.xyz/assets/images/logos_s.png",
          "sizes": "168x168",
          "type": "image/png"
        },
        {
          src: "https://sparkonline.xyz/assets/images/logos_s.png", // <== don't add slash, for testing
          sizes: "192x192",
          type: "image/png",
        },
        {
          src: "https://sparkonline.xyz/assets/images/logos_s.png", // <== don't remove slash, for testing
          sizes: "512x512",
          type: "image/png",
        },
        {
          src: "https://sparkonline.xyz/assets/images/logos_s.png", // <== don't add slash, for testing
          sizes: "512x512",
          type: "image/png",
          purpose: "any maskable",
        },
      ],
    },
  }),
  ],
  define: {
    'window.global': {},
    'process.env': {},

  },
  server: {
    port: 2899,
    strictPort: true,
    host: true,
  },
  optimizeDeps: {
    esbuildOptions: {
      plugins: [
      ],
    }
  },
  build: {
    rollupOptions: {
      plugins: [
        nodePolyfills({ crypto: true }),
        NodeGlobalsPolyfillPlugin({ buffer: true }),
      ],
    }
  },
  resolve: {
    alias: {
      stream: 'rollup-plugin-node-polyfills/polyfills/stream',
      events: 'rollup-plugin-node-polyfills/polyfills/events',
      assert: 'assert',
      crypto: 'crypto-browserify',
      util: 'util',
      'near-api-js': 'near-api-js/dist/near-api-js.js',
      '@': fileURLToPath(new URL('./src', import.meta.url)),
      '@store': fileURLToPath(new URL('./src/store', import.meta.url)),
    },
    dedupe: ['vue'],
  },
});
