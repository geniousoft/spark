import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/micro",
      name: "micro",
      component: () => import("../views/MicroCampainsView.vue"),
    },
    {
      path: "/nano",
      name: "nano",
      component: () => import("../views/NanoCampainsView.vue"),
    },
    {
      path: "/add-link",
      name: "addlink",
      component: () => import("../views/forms/AddLink.vue"),
    },
    {
      path: "/detail/:type/:id",
      name: "detail",
      component: () => import("../views/forms/Detail.vue"),
    },
    
    {
      path: "/campaigns",
      name: "campaign",
      component: () => import("../views/campaign/View.vue"),
    },
    {
      path: "/campaigns/:id",
      name: "campaignDetail",
      component: () => import("../views/campaign/Detail.vue"),
    },
    {
      path: '/create',
      component: () => import("../views/forms/CreateSelect.vue"),
      children: [
        {
          path: '',
          component: () => import("../views/SelectCampain.vue"),
        },
        {
          path: 'nano',
          component: () => import("../views/forms/CreateNano.vue"),
        },
        {
          path: 'micro',
          component: () => import("../views/forms/CreateMicro.vue"),
        },
        {
          path: 'comunity-gated',
          component: () => import("../views/forms/CreateCommunity.vue"),
        }
      ]
    },
    {
      path: '/brands',
      children: [
        {
          path: '',
          component: () => import("../views/brands/View.vue"),
        },
        {
          path: 'create',
          name:'brandsCreate',
          component: () => import("../views/brands/Create.vue"),
        },
        {
          path: ':id/edit',
          name:'brandsEdit',
          component: () => import("../views/brands/Edit.vue"),
        }
      ]
    },
    {
      path: "/community-gated",
      name: "communityGated",
      component: () => import("../views/CommuntyGatedCampainsView.vue"),
    },
    {
      path: "/support",
      name: "support",
      component: () => import("../views/Support.vue"),
    },
    {
      path: "/faqs",
      name: "faqs",
      component: () => import("../views/Faqs.vue"),
    },
    {
      path: "/profile",
      name: "profile",
      component: () => import("../views/Profile.vue"),
    },
    {
      path: "/road",
      name: "road",
      component: () => import("../views/RoadMap.vue"),
    },
    {
      path: "/about",
      name: "about",
      component: () => import("../views/AboutView.vue"),
    },
  ],
});

export default router;
