import { createApp , provide, h } from "vue";
import { createPinia } from "pinia";
import App from "./App.vue";
import router from "./router";
import "./assets/main.css";
import SolanaWallets from "solana-wallets-vue";
import "solana-wallets-vue/styles.css";
import { WalletAdapterNetwork } from "@solana/wallet-adapter-base";
import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client/core'
import { createApolloProvider } from '@vue/apollo-option'
import { DefaultApolloClient } from '@vue/apollo-composable'
import { onError } from '@apollo/client/link/error'
import Vue3Toastify, { toast } from 'vue3-toastify';

import {
  PhantomWalletAdapter,
  SlopeWalletAdapter,
  SolflareWalletAdapter,
  BraveWalletAdapter
} from "@solana/wallet-adapter-wallets";

const httpLink = createHttpLink({
  uri: 'https://api.sparkonline.xyz/graphql',
})

const cache = new InMemoryCache()

const errorLink = onError(error => {
    for (let index = 0; index < error.response.errors.length; index++) {
      const element = error.response.errors[index];
      if(element.debugMessage){
        toast.error(element.debugMessage);
      }else{
        toast.error(element.message);
      }
    }
})
const apolloClient = new ApolloClient({
  link: errorLink.concat(httpLink),
  cache,
})
const apolloProvider = createApolloProvider({
  defaultClient: apolloClient,
})


const walletOptions = {
  wallets: [
    new PhantomWalletAdapter(),
    new SlopeWalletAdapter(),
    new SolflareWalletAdapter({ network: WalletAdapterNetwork.Mainnet }),
    new BraveWalletAdapter()
  ],
  autoConnect: true,
};

const app = createApp({
  setup () {
    provide(DefaultApolloClient, apolloClient)
  },
  render: () => h(App),
})

app.use(createPinia());
app.use(router);
app.use(SolanaWallets, walletOptions);
app.use(apolloProvider)
app.use(Vue3Toastify)
app.mount("#app");
