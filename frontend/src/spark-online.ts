export type SparkOnline = {
  "version": "0.1.0",
  "name": "spark_online",
  "instructions": [
    {
      "name": "campaingCreate",
      "accounts": [
        {
          "name": "owner",
          "isMut": true,
          "isSigner": true
        },
        {
          "name": "campaing",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "systemProgram",
          "isMut": false,
          "isSigner": false
        }
      ],
      "args": [
        {
          "name": "campaingCode",
          "type": "string"
        },
        {
          "name": "description",
          "type": "string"
        },
        {
          "name": "url",
          "type": "string"
        },
        {
          "name": "title",
          "type": "string"
        },
        {
          "name": "perPay",
          "type": "u64"
        }
      ]
    },
    {
      "name": "updateCampaing",
      "accounts": [
        {
          "name": "campaing",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "owner",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "user",
          "isMut": false,
          "isSigner": true
        }
      ],
      "args": [
        {
          "name": "campaingCode",
          "type": "string"
        },
        {
          "name": "description",
          "type": "string"
        },
        {
          "name": "url",
          "type": "string"
        },
        {
          "name": "title",
          "type": "string"
        }
      ]
    },
    {
      "name": "deposite",
      "accounts": [
        {
          "name": "user",
          "isMut": true,
          "isSigner": true
        },
        {
          "name": "campaing",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "owner",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "systemProgram",
          "isMut": false,
          "isSigner": false
        }
      ],
      "args": [
        {
          "name": "campaingCode",
          "type": "string"
        },
        {
          "name": "amount",
          "type": "u64"
        }
      ]
    },
    {
      "name": "cancel",
      "accounts": [
        {
          "name": "campaing",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "owner",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "user",
          "isMut": false,
          "isSigner": true
        }
      ],
      "args": [
        {
          "name": "campaingCode",
          "type": "string"
        }
      ]
    },
    {
      "name": "close",
      "accounts": [
        {
          "name": "campaing",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "owner",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "user",
          "isMut": false,
          "isSigner": true
        }
      ],
      "args": [
        {
          "name": "campaingCode",
          "type": "string"
        }
      ]
    },
    {
      "name": "distrubutePlayer",
      "accounts": [
        {
          "name": "user",
          "isMut": true,
          "isSigner": true
        },
        {
          "name": "campaing",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "owner",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "player",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "controllers",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "systemProgram",
          "isMut": false,
          "isSigner": false
        }
      ],
      "args": [
        {
          "name": "campaingCode",
          "type": "string"
        }
      ]
    },
    {
      "name": "addController",
      "accounts": [
        {
          "name": "signer",
          "isMut": true,
          "isSigner": true
        },
        {
          "name": "controllers",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "program",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "programData",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "systemProgram",
          "isMut": false,
          "isSigner": false
        }
      ],
      "args": [
        {
          "name": "key",
          "type": "publicKey"
        }
      ]
    },
    {
      "name": "removeController",
      "accounts": [
        {
          "name": "signer",
          "isMut": true,
          "isSigner": true
        },
        {
          "name": "controllers",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "program",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "programData",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "systemProgram",
          "isMut": false,
          "isSigner": false
        }
      ],
      "args": [
        {
          "name": "key",
          "type": "publicKey"
        }
      ]
    }
  ],
  "accounts": [
    {
      "name": "controller",
      "type": {
        "kind": "struct",
        "fields": [
          {
            "name": "list",
            "type": {
              "vec": "publicKey"
            }
          }
        ]
      }
    },
    {
      "name": "campaingList",
      "type": {
        "kind": "struct",
        "fields": [
          {
            "name": "description",
            "type": "string"
          },
          {
            "name": "url",
            "type": "string"
          },
          {
            "name": "title",
            "type": "string"
          },
          {
            "name": "owner",
            "type": "publicKey"
          },
          {
            "name": "vault",
            "type": "u64"
          },
          {
            "name": "perPay",
            "type": "u64"
          },
          {
            "name": "campaingCode",
            "type": "string"
          },
          {
            "name": "status",
            "type": "bool"
          },
          {
            "name": "participants",
            "type": {
              "vec": "publicKey"
            }
          }
        ]
      }
    }
  ],
  "errors": [
    {
      "code": 6000,
      "name": "WrongListOwner",
      "msg": "Specified campaing owner does not match the pubkey in the campaing"
    },
    {
      "code": 6001,
      "name": "Unauthorized",
      "msg": "Unauthorized"
    },
    {
      "code": 6002,
      "name": "CampaingIsClose",
      "msg": "campaing is close"
    },
    {
      "code": 6003,
      "name": "BountyTooSmall",
      "msg": "BountyTooSmall"
    },
    {
      "code": 6004,
      "name": "NoMoneyInCampaing",
      "msg": "No sol in left campaing"
    },
    {
      "code": 6005,
      "name": "InfB",
      "msg": "insufficient balance"
    }
  ]
};

export const IDL: SparkOnline = {
  "version": "0.1.0",
  "name": "spark_online",
  "instructions": [
    {
      "name": "campaingCreate",
      "accounts": [
        {
          "name": "owner",
          "isMut": true,
          "isSigner": true
        },
        {
          "name": "campaing",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "systemProgram",
          "isMut": false,
          "isSigner": false
        }
      ],
      "args": [
        {
          "name": "campaingCode",
          "type": "string"
        },
        {
          "name": "description",
          "type": "string"
        },
        {
          "name": "url",
          "type": "string"
        },
        {
          "name": "title",
          "type": "string"
        },
        {
          "name": "perPay",
          "type": "u64"
        }
      ]
    },
    {
      "name": "updateCampaing",
      "accounts": [
        {
          "name": "campaing",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "owner",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "user",
          "isMut": false,
          "isSigner": true
        }
      ],
      "args": [
        {
          "name": "campaingCode",
          "type": "string"
        },
        {
          "name": "description",
          "type": "string"
        },
        {
          "name": "url",
          "type": "string"
        },
        {
          "name": "title",
          "type": "string"
        }
      ]
    },
    {
      "name": "deposite",
      "accounts": [
        {
          "name": "user",
          "isMut": true,
          "isSigner": true
        },
        {
          "name": "campaing",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "owner",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "systemProgram",
          "isMut": false,
          "isSigner": false
        }
      ],
      "args": [
        {
          "name": "campaingCode",
          "type": "string"
        },
        {
          "name": "amount",
          "type": "u64"
        }
      ]
    },
    {
      "name": "cancel",
      "accounts": [
        {
          "name": "campaing",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "owner",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "user",
          "isMut": false,
          "isSigner": true
        }
      ],
      "args": [
        {
          "name": "campaingCode",
          "type": "string"
        }
      ]
    },
    {
      "name": "close",
      "accounts": [
        {
          "name": "campaing",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "owner",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "user",
          "isMut": false,
          "isSigner": true
        }
      ],
      "args": [
        {
          "name": "campaingCode",
          "type": "string"
        }
      ]
    },
    {
      "name": "distrubutePlayer",
      "accounts": [
        {
          "name": "user",
          "isMut": true,
          "isSigner": true
        },
        {
          "name": "campaing",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "owner",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "player",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "controllers",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "systemProgram",
          "isMut": false,
          "isSigner": false
        }
      ],
      "args": [
        {
          "name": "campaingCode",
          "type": "string"
        }
      ]
    },
    {
      "name": "addController",
      "accounts": [
        {
          "name": "signer",
          "isMut": true,
          "isSigner": true
        },
        {
          "name": "controllers",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "program",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "programData",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "systemProgram",
          "isMut": false,
          "isSigner": false
        }
      ],
      "args": [
        {
          "name": "key",
          "type": "publicKey"
        }
      ]
    },
    {
      "name": "removeController",
      "accounts": [
        {
          "name": "signer",
          "isMut": true,
          "isSigner": true
        },
        {
          "name": "controllers",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "program",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "programData",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "systemProgram",
          "isMut": false,
          "isSigner": false
        }
      ],
      "args": [
        {
          "name": "key",
          "type": "publicKey"
        }
      ]
    }
  ],
  "accounts": [
    {
      "name": "controller",
      "type": {
        "kind": "struct",
        "fields": [
          {
            "name": "list",
            "type": {
              "vec": "publicKey"
            }
          }
        ]
      }
    },
    {
      "name": "campaingList",
      "type": {
        "kind": "struct",
        "fields": [
          {
            "name": "description",
            "type": "string"
          },
          {
            "name": "url",
            "type": "string"
          },
          {
            "name": "title",
            "type": "string"
          },
          {
            "name": "owner",
            "type": "publicKey"
          },
          {
            "name": "vault",
            "type": "u64"
          },
          {
            "name": "perPay",
            "type": "u64"
          },
          {
            "name": "campaingCode",
            "type": "string"
          },
          {
            "name": "status",
            "type": "bool"
          },
          {
            "name": "participants",
            "type": {
              "vec": "publicKey"
            }
          }
        ]
      }
    }
  ],
  "errors": [
    {
      "code": 6000,
      "name": "WrongListOwner",
      "msg": "Specified campaing owner does not match the pubkey in the campaing"
    },
    {
      "code": 6001,
      "name": "Unauthorized",
      "msg": "Unauthorized"
    },
    {
      "code": 6002,
      "name": "CampaingIsClose",
      "msg": "campaing is close"
    },
    {
      "code": 6003,
      "name": "BountyTooSmall",
      "msg": "BountyTooSmall"
    },
    {
      "code": 6004,
      "name": "NoMoneyInCampaing",
      "msg": "No sol in left campaing"
    },
    {
      "code": 6005,
      "name": "InfB",
      "msg": "insufficient balance"
    }
  ]
};
