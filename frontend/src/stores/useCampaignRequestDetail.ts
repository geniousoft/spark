import { defineStore } from 'pinia';
import { ref } from 'vue';

export const useCampaignRequestDetail = defineStore('detail', () => {
  const campaign:any = ref();
  function set(item) {
    campaign.value = item;
  }
  return { campaign, set };
});
