import { defineStore } from 'pinia';
import * as anchor from '@project-serum/anchor';
import { Program } from '@project-serum/anchor';
import { Connection, clusterApiUrl, LAMPORTS_PER_SOL, PublicKey } from '@solana/web3.js';
import { IDL } from '../spark-online';
import { useAnchorWallet } from 'solana-wallets-vue';
import { ConfirmOptions } from 'solana';
import { Buffer } from 'buffer';
import { toast } from 'vue3-toastify';
import { computed, ref, onMounted, reactive } from 'vue';

type CreateCampaigntInput = {
  type: Number;
  title: string;
  project_id: Number;
  description: string;
  data: string;
  per_pay: number;
  code: string;
  pubkey: string;
  system_wallet: any;
  project: any;
};
export const useSparkleStore = defineStore('sparkle', () => {
  const programID: any = new PublicKey('8Ek6a9WTeKsUiiLKPWEjn1x8ScRrniHcdHP8h2gcNcoc');
  const programData: any = reactive({
    participants: [],
    status: false,
    vault: 0,
  });

  const balance = ref(0);
  const wallet = useAnchorWallet();
  const opts = {
    preflightCommitment: 'processed',
  };
  const network = clusterApiUrl('devnet');
  const connection = new Connection(network);
  const provider = new anchor.AnchorProvider(connection, wallet.value as anchor.Wallet, opts.preflightCommitment as ConfirmOptions);
  const program = new Program(IDL, programID, provider);

  async function campaignPDA(name: string) {
    anchor.setProvider(provider);
    const [campaign, bump] = anchor.web3.PublicKey.findProgramAddressSync([Buffer.from('campaing'), provider.publicKey.toBuffer(), Buffer.from(name.slice(0, 32))], programID);
    return campaign;
  }
  async function creatCampaign(data: CreateCampaigntInput) {
    const pdaAccount = await campaignPDA(data.code);
    let link = 'https://dashboard.sparkonline.xyz/detail/'+data.type+'/'+ data.code;
    const tx = await program.methods
      .campaingCreate(data.code, data.description, link , data.title, new anchor.BN(data.per_pay * LAMPORTS_PER_SOL))
      .accounts({
        owner: provider.wallet.publicKey,
        campaing: pdaAccount,
        systemProgram: anchor.web3.SystemProgram.programId,
      })
      .transaction();
    let blockhashObj = await connection.getRecentBlockhash();
    tx.recentBlockhash = await blockhashObj.blockhash;
    tx.feePayer = provider.wallet.publicKey;
    const txHash = await provider.wallet.signTransaction(tx);
    let signature = await connection.sendRawTransaction(txHash.serialize());
    await connection.confirmTransaction(signature);
    toast.success('Your campaign is deployed on-chain');
    return pdaAccount;
  }
  async function deposite(code, deposite, pda) {
    let depositePrice = LAMPORTS_PER_SOL * deposite;
    let account = {
      user: provider.wallet.publicKey,
      owner: provider.wallet.publicKey,
      campaing: new PublicKey(pda),
      systemProgram: anchor.web3.SystemProgram.programId,
    };
    const tx = await program.methods.deposite(code, new anchor.BN(depositePrice)).accounts(account).transaction();
    let blockhashObj = await connection.getRecentBlockhash();
    tx.recentBlockhash = await blockhashObj.blockhash;
    tx.feePayer = provider.wallet.publicKey;
    const txHash = await provider.wallet.signTransaction(tx);
    let signature = await connection.sendRawTransaction(txHash.serialize());
    await connection.confirmTransaction(signature);
    toast.success('Deposit Successful');
    await getBalance(pda);
    await getData(pda);
    return code;
  }
  async function close(code, pda) {
    let account = {
      user: provider.wallet.publicKey,
      owner: provider.wallet.publicKey,
      campaing: new PublicKey(pda),
      systemProgram: anchor.web3.SystemProgram.programId,
    };
    const tx = await program.methods.close(code).accounts(account).transaction();
    let blockhashObj = await connection.getRecentBlockhash();
    tx.recentBlockhash = await blockhashObj.blockhash;
    tx.feePayer = provider.wallet.publicKey;
    const txHash = await provider.wallet.signTransaction(tx);
    let signature = await connection.sendRawTransaction(txHash.serialize());
    await connection.confirmTransaction(signature);
    toast.success('Your campaign deleted on-chain');
    return code;
  }

  async function getData(key) {
    const pdaData = await program.account.campaingList.fetch(key);
    programData.participants = pdaData.participants;
    programData.status = pdaData.status;
    programData.vault = (pdaData.vault.toNumber() / LAMPORTS_PER_SOL).toFixed(3);
    getBalance(key);
    return pdaData;
  }
  async function getBalance(key) {
    let k = new PublicKey(key);
    const pdaBalance = await provider.connection.getBalance(k);
    balance.value = (pdaBalance / LAMPORTS_PER_SOL).toFixed(3);
    return  (pdaBalance / LAMPORTS_PER_SOL).toFixed(3);
  }

  return {
    wallet,
    provider,
    programID,
    program,
    connection,
    balance,
    programData,
    creatCampaign,
    getData,
    deposite,
    getBalance,
    close
  };
});
