import axios from 'axios';
import { defineStore } from 'pinia';

export const useRoadMapStore = defineStore('roadmaps', {
    state: () => ({
        roadmaps: [],
    }),
    getters: {
        getRoadMaps(state) {
            return state.roadmaps
        }
    },
    actions: {
        async fetchRoadMaps() {
            try {
                const data = await axios.post('https://api.sparkonline.xyz/graphql',
                    {
                        query: '{roadmaps(orderBy:{ column : CREATED_AT, order:ASC }){ title description date activated }}'
                    }
                )
                this.roadmaps = data.data.data.roadmaps
                console.log(this.roadmaps ,data);
            }
            catch (error) {
                console.log(error)
            }
        }
    },
})
