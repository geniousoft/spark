import { defineStore } from 'pinia';
import { reactive, ref } from 'vue';

export const useProjectStore = defineStore('projects', () => {
  const brands:any = ref(0);
  function set(item) {
    brands.value = item;
  }
  function get() {
    return brands.value 
  }
  return { brands, set,get };
});
