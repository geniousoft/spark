import { defineStore } from 'pinia';
import { reactive, ref } from 'vue';

export const useCampaignDetail = defineStore('campaigndetail', () => {
  const campaign:any = ref(0);
  function set(item) {
    campaign.value = item;
  }
  function get() {
    return campaign.value 
  }
  return { campaign, set,get };
});
