<?php

return [
    'admin-user' => [
        'title' => 'Users',

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
            'edit_profile' => 'Edit Profile',
            'edit_password' => 'Edit Password',
        ],

        'columns' => [
            'id' => 'ID',
            'last_login_at' => 'Last login',
            'activated' => 'Activated',
            'email' => 'Email',
            'first_name' => 'First name',
            'forbidden' => 'Forbidden',
            'language' => 'Language',
            'last_name' => 'Last name',
            'password' => 'Password',
            'password_repeat' => 'Password Confirmation',
                
            //Belongs to many relations
            'roles' => 'Roles',
                
        ],
    ],

    'road-map' => [
        'title' => 'Roadmap',

        'actions' => [
            'index' => 'Roadmap',
            'create' => 'New Roadmap',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            
        ],
    ],

    'roadmap' => [
        'title' => 'Roadmaps',

        'actions' => [
            'index' => 'Roadmaps',
            'create' => 'New Roadmap',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            
        ],
    ],

    'roadmap' => [
        'title' => 'Roadmap',

        'actions' => [
            'index' => 'Roadmap',
            'create' => 'New Roadmap',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'activated' => 'Activated',
            'date' => 'Date',
            'description' => 'Description',
            'order' => 'Order',
            'title' => 'Title',
            
        ],
    ],

    'profile' => [
        'title' => 'Profiles',

        'actions' => [
            'index' => 'Profiles',
            'create' => 'New Profile',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'twitter_avatar' => 'Twitter avatar',
            'twitter_handle' => 'Twitter handle',
            'twitter_token' => 'Twitter token',
            'twitter_id' => 'Twitter',
            'system_wallet' => 'System wallet',
            'payment_wallet' => 'Payment wallet',
            'category' => 'Category',
            'follower' => 'Follower',
            
        ],
    ],

    'project' => [
        'title' => 'Projects',

        'actions' => [
            'index' => 'Projects',
            'create' => 'New Project',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'twitter' => 'Twitter',
            'discord' => 'Discord',
            'link' => 'Link',
            'name' => 'Name',
            'description' => 'Description',
            'logo' => 'Logo',
            
        ],
    ],

    'campaign' => [
        'title' => 'Campaigns',

        'actions' => [
            'index' => 'Campaigns',
            'create' => 'New Campaign',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'type' => 'Type',
            'title' => 'Title',
            'project_id' => 'Project',
            'description' => 'Description',
            'data' => 'Data',
            'per_pay' => 'Per pay',
            'code' => 'Code',
            'enabled' => 'Enabled',
            'pubkey' => 'Pubkey',
            
        ],
    ],

    'campaign-request' => [
        'title' => 'Campaign Requests',

        'actions' => [
            'index' => 'Campaign Requests',
            'create' => 'New Campaign Request',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'campaign_id' => 'Campaign',
            'data' => 'Data',
            'payment_wallet' => 'Payment wallet',
            'project_id' => 'Project',
            'status' => 'Status',
            'point' => 'Point',
            'not' => 'Not',
            'type' => 'Type',
            'prize' => 'Prize',
            
        ],
    ],

    'profile-stat' => [
        'title' => 'Profile Stats',

        'actions' => [
            'index' => 'Profile Stats',
            'create' => 'New Profile Stat',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'project_id' => 'Project',
            'total_apply' => 'Total apply',
            'total_follower' => 'Total follower',
            'total_worth' => 'Total worth',
            'total_point' => 'Total point',
            'total_expectant' => 'Total expectant',
            
        ],
    ],

    'campaign-stat' => [
        'title' => 'Campaign Stats',

        'actions' => [
            'index' => 'Campaign Stats',
            'create' => 'New Campaign Stat',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'campaign_id' => 'Campaign',
            'total_apply' => 'Total apply',
            'total_pool' => 'Total pool',
            'total_prize' => 'Total prize',
            'average_point' => 'Average point',
            
        ],
    ],

    'n-f-t-collection' => [
        'title' => 'N F T Collections',

        'actions' => [
            'index' => 'N F T Collections',
            'create' => 'New N F T Collection',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'helloMoonCollectionId' => 'HelloMoonCollectionId',
            'image' => 'Image',
            'name' => 'Name',
            'supply' => 'Supply',
            
        ],
    ],

    'nft-collection' => [
        'title' => 'Nft Collections',

        'actions' => [
            'index' => 'Nft Collections',
            'create' => 'New Nft Collection',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'helloMoonCollectionId' => 'HelloMoonCollectionId',
            'image' => 'Image',
            'name' => 'Name',
            'supply' => 'Supply',
            
        ],
    ],

    'twit-link' => [
        'title' => 'Twit Links',

        'actions' => [
            'index' => 'Twit Links',
            'create' => 'New Twit Link',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'twit_id' => 'Twit',
            
        ],
    ],

    // Do not delete me :) I'm used for auto-generation
];