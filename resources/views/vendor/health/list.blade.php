<html lang="en" class="dark">

<head>
    <title>Spark API</title>
    <meta name="description" content="A new platform for community focused, token-gated
    micro-influencer marketing">

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="title" content="Spark API">

    <meta name="language" content="English">
    <meta name="author" content="vitrulabs">



    <!-- Favicon and Touch Icons  -->
    <link rel="shortcut icon" href="https://sparkonline.xyz/assets/images/logos_s.png">
    <link rel="apple-touch-icon-precomposed" href="assets/images/logos_s.png">
    <!-- Primary Meta Tags -->

    <!-- Facebook Meta Tags -->
    <meta property="og:url" content="https://sparkonline.xyz/">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Spark API">
    <meta property="og:description" content="A new platform for community focused, token-gated
        micro-influencer marketing">
    <meta property="og:image" content="https://sparkonline.xyz/splash.png">

    <!-- Twitter Meta Tags -->
    <meta name="twitter:card" content="summary_large_image">
    <meta property="twitter:domain" content="sparkonline.xyz">
    <meta property="twitter:url" content="https://sparkonline.xyz/">
    <meta name="twitter:title" content="Spark API">
    <meta name="twitter:description" content="A new platform for community focused, token-gated
        micro-influencer marketing">
    <meta name="twitter:image" content="https://sparkonline.xyz/splash.png">
    <script src="https://cdn.tailwindcss.com"></script>

    {{$assets}}
</head>

<body class="antialiased bg-gray-100 mt-7 md:mt-12 dark:bg-gray-900">

    <div class="mx-auto max-w-7xl lg:px-8 sm:px-6">
        <div class="flex flex-wrap justify-center space-y-3">
            <h4 class="w-full text-2xl font-bold text-center text-gray-900 dark:text-white">Spark Online Health</h4>
            <div class="flex justify-center w-full">
                <x-health-logo />
            </div>
            @if ($lastRanAt)
            <div class="{{ $lastRanAt->diffInMinutes() > 5 ? 'text-red-400' : 'text-gray-400 dark:text-gray-500' }} text-sm text-center font-medium">
                {{ __('health::notifications.check_results_from') }} {{ $lastRanAt->diffForHumans() }}
            </div>
            @endif
        </div>

        <div class="px-2 mt-6 md:mt-8 md:px-0 ">
            @if (count($checkResults?->storedCheckResults ?? []))
            <dl class=" grid grid-cols-1 gap-2.5 sm:gap-3 md:gap-5 md:grid-cols-2 lg:grid-cols-3">
                @foreach ($checkResults->storedCheckResults as $result)

                <div class="flex items-start px-4 space-x-2 overflow-hidden py-5 text-opacity-0 transition transform bg-white shadow-md shadow-gray-200 dark:shadow-black/25 dark:shadow-md dark:bg-gray-800 rounded-xl sm:p-6 md:space-x-3 md:min-h-[130px] dark:border-t dark:border-gray-700">

                    <x-health-status-indicator :result="$result" />
                    <div>
                        <dd class="-mt-1 font-bold text-gray-900 dark:text-white md:mt-1 md:text-xl">
                            {{ $result->label }}

                        </dd>
                        <dt class="mt-0 text-sm font-medium text-gray-600 dark:text-gray-300 md:mt-1">
                            @if (!empty($result->notificationMessage))
                            {{ $result->notificationMessage }}
                            @else
                            {{ $result->shortSummary }}
                            @endif
                        </dt>
                    </div>
                </div>
                @endforeach
            </dl>
            @endif
        </div>
    </div>
</body>

</html>