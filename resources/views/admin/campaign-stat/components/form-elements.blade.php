<div class="form-group row align-items-center" :class="{'has-danger': errors.has('campaign_id'), 'has-success': fields.campaign_id && fields.campaign_id.valid }">
    <label for="campaign_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.campaign-stat.columns.campaign_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.campaign_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('campaign_id'), 'form-control-success': fields.campaign_id && fields.campaign_id.valid}" id="campaign_id" name="campaign_id" placeholder="{{ trans('admin.campaign-stat.columns.campaign_id') }}">
        <div v-if="errors.has('campaign_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('campaign_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('total_apply'), 'has-success': fields.total_apply && fields.total_apply.valid }">
    <label for="total_apply" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.campaign-stat.columns.total_apply') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.total_apply" v-validate="'required|integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('total_apply'), 'form-control-success': fields.total_apply && fields.total_apply.valid}" id="total_apply" name="total_apply" placeholder="{{ trans('admin.campaign-stat.columns.total_apply') }}">
        <div v-if="errors.has('total_apply')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('total_apply') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('total_pool'), 'has-success': fields.total_pool && fields.total_pool.valid }">
    <label for="total_pool" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.campaign-stat.columns.total_pool') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.total_pool" v-validate="'required|integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('total_pool'), 'form-control-success': fields.total_pool && fields.total_pool.valid}" id="total_pool" name="total_pool" placeholder="{{ trans('admin.campaign-stat.columns.total_pool') }}">
        <div v-if="errors.has('total_pool')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('total_pool') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('total_prize'), 'has-success': fields.total_prize && fields.total_prize.valid }">
    <label for="total_prize" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.campaign-stat.columns.total_prize') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.total_prize" v-validate="'required|integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('total_prize'), 'form-control-success': fields.total_prize && fields.total_prize.valid}" id="total_prize" name="total_prize" placeholder="{{ trans('admin.campaign-stat.columns.total_prize') }}">
        <div v-if="errors.has('total_prize')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('total_prize') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('average_point'), 'has-success': fields.average_point && fields.average_point.valid }">
    <label for="average_point" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.campaign-stat.columns.average_point') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.average_point" v-validate="'required|integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('average_point'), 'form-control-success': fields.average_point && fields.average_point.valid}" id="average_point" name="average_point" placeholder="{{ trans('admin.campaign-stat.columns.average_point') }}">
        <div v-if="errors.has('average_point')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('average_point') }}</div>
    </div>
</div>


