<div class="form-group row align-items-center" :class="{'has-danger': errors.has('twit_id'), 'has-success': fields.twit_id && fields.twit_id.valid }">
    <label for="twit_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.twit-link.columns.twit_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.twit_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('twit_id'), 'form-control-success': fields.twit_id && fields.twit_id.valid}" id="twit_id" name="twit_id" placeholder="{{ trans('admin.twit-link.columns.twit_id') }}">
        <div v-if="errors.has('twit_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('twit_id') }}</div>
    </div>
</div>


