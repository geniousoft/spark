<div class="form-group row align-items-center" :class="{'has-danger': errors.has('twitter_avatar'), 'has-success': fields.twitter_avatar && fields.twitter_avatar.valid }">
    <label for="twitter_avatar" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.profile.columns.twitter_avatar') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.twitter_avatar" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('twitter_avatar'), 'form-control-success': fields.twitter_avatar && fields.twitter_avatar.valid}" id="twitter_avatar" name="twitter_avatar" placeholder="{{ trans('admin.profile.columns.twitter_avatar') }}">
        <div v-if="errors.has('twitter_avatar')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('twitter_avatar') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('twitter_handle'), 'has-success': fields.twitter_handle && fields.twitter_handle.valid }">
    <label for="twitter_handle" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.profile.columns.twitter_handle') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.twitter_handle" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('twitter_handle'), 'form-control-success': fields.twitter_handle && fields.twitter_handle.valid}" id="twitter_handle" name="twitter_handle" placeholder="{{ trans('admin.profile.columns.twitter_handle') }}">
        <div v-if="errors.has('twitter_handle')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('twitter_handle') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('twitter_token'), 'has-success': fields.twitter_token && fields.twitter_token.valid }">
    <label for="twitter_token" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.profile.columns.twitter_token') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.twitter_token" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('twitter_token'), 'form-control-success': fields.twitter_token && fields.twitter_token.valid}" id="twitter_token" name="twitter_token" placeholder="{{ trans('admin.profile.columns.twitter_token') }}">
        <div v-if="errors.has('twitter_token')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('twitter_token') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('twitter_id'), 'has-success': fields.twitter_id && fields.twitter_id.valid }">
    <label for="twitter_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.profile.columns.twitter_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.twitter_id" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('twitter_id'), 'form-control-success': fields.twitter_id && fields.twitter_id.valid}" id="twitter_id" name="twitter_id" placeholder="{{ trans('admin.profile.columns.twitter_id') }}">
        <div v-if="errors.has('twitter_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('twitter_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('system_wallet'), 'has-success': fields.system_wallet && fields.system_wallet.valid }">
    <label for="system_wallet" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.profile.columns.system_wallet') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.system_wallet" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('system_wallet'), 'form-control-success': fields.system_wallet && fields.system_wallet.valid}" id="system_wallet" name="system_wallet" placeholder="{{ trans('admin.profile.columns.system_wallet') }}">
        <div v-if="errors.has('system_wallet')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('system_wallet') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('payment_wallet'), 'has-success': fields.payment_wallet && fields.payment_wallet.valid }">
    <label for="payment_wallet" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.profile.columns.payment_wallet') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.payment_wallet" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('payment_wallet'), 'form-control-success': fields.payment_wallet && fields.payment_wallet.valid}" id="payment_wallet" name="payment_wallet" placeholder="{{ trans('admin.profile.columns.payment_wallet') }}">
        <div v-if="errors.has('payment_wallet')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('payment_wallet') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('category'), 'has-success': fields.category && fields.category.valid }">
    <label for="category" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.profile.columns.category') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.category" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('category'), 'form-control-success': fields.category && fields.category.valid}" id="category" name="category" placeholder="{{ trans('admin.profile.columns.category') }}">
        <div v-if="errors.has('category')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('category') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('follower'), 'has-success': fields.follower && fields.follower.valid }">
    <label for="follower" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.profile.columns.follower') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.follower" v-validate="'integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('follower'), 'form-control-success': fields.follower && fields.follower.valid}" id="follower" name="follower" placeholder="{{ trans('admin.profile.columns.follower') }}">
        <div v-if="errors.has('follower')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('follower') }}</div>
    </div>
</div>


