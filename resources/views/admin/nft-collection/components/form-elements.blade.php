<div class="form-group row align-items-center" :class="{'has-danger': errors.has('helloMoonCollectionId'), 'has-success': fields.helloMoonCollectionId && fields.helloMoonCollectionId.valid }">
    <label for="helloMoonCollectionId" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.nft-collection.columns.helloMoonCollectionId') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.helloMoonCollectionId" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('helloMoonCollectionId'), 'form-control-success': fields.helloMoonCollectionId && fields.helloMoonCollectionId.valid}" id="helloMoonCollectionId" name="helloMoonCollectionId" placeholder="{{ trans('admin.nft-collection.columns.helloMoonCollectionId') }}">
        <div v-if="errors.has('helloMoonCollectionId')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('helloMoonCollectionId') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('image'), 'has-success': fields.image && fields.image.valid }">
    <label for="image" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.nft-collection.columns.image') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.image" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('image'), 'form-control-success': fields.image && fields.image.valid}" id="image" name="image" placeholder="{{ trans('admin.nft-collection.columns.image') }}">
        <div v-if="errors.has('image')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('image') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('name'), 'has-success': fields.name && fields.name.valid }">
    <label for="name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.nft-collection.columns.name') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.name" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('name'), 'form-control-success': fields.name && fields.name.valid}" id="name" name="name" placeholder="{{ trans('admin.nft-collection.columns.name') }}">
        <div v-if="errors.has('name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('name') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('supply'), 'has-success': fields.supply && fields.supply.valid }">
    <label for="supply" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.nft-collection.columns.supply') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.supply" v-validate="'required|integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('supply'), 'form-control-success': fields.supply && fields.supply.valid}" id="supply" name="supply" placeholder="{{ trans('admin.nft-collection.columns.supply') }}">
        <div v-if="errors.has('supply')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('supply') }}</div>
    </div>
</div>


