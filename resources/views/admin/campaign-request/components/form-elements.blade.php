<div class="row form-inline" style="padding-bottom: 10px;" v-cloak>
    <div :class="{'col-xl-10 col-md-11 text-right': !isFormLocalized, 'col text-center': isFormLocalized, 'hidden': onSmallScreen }">
        <small>{{ trans('brackets/admin-ui::admin.forms.currently_editing_translation') }}<span v-if="!isFormLocalized && otherLocales.length > 1"> {{ trans('brackets/admin-ui::admin.forms.more_can_be_managed') }}</span><span v-if="!isFormLocalized"> | <a href="#" @click.prevent="showLocalization">{{ trans('brackets/admin-ui::admin.forms.manage_translations') }}</a></span></small>
        <i class="localization-error" v-if="!isFormLocalized && showLocalizedValidationError"></i>
    </div>

    <div class="col text-center" :class="{'language-mobile': onSmallScreen, 'has-error': !isFormLocalized && showLocalizedValidationError}" v-if="isFormLocalized || onSmallScreen" v-cloak>
        <small>{{ trans('brackets/admin-ui::admin.forms.choose_translation_to_edit') }}
            <select class="form-control" v-model="currentLocale">
                <option :value="defaultLocale" v-if="onSmallScreen">@{{defaultLocale.toUpperCase()}}</option>
                <option v-for="locale in otherLocales" :value="locale">@{{locale.toUpperCase()}}</option>
            </select>
            <i class="localization-error" v-if="isFormLocalized && showLocalizedValidationError"></i>
            <span>|</span>
            <a href="#" @click.prevent="hideLocalization">{{ trans('brackets/admin-ui::admin.forms.hide') }}</a>
        </small>
    </div>
</div>


<div class="form-group row align-items-center" :class="{'has-danger': errors.has('data'), 'has-success': fields.data && fields.data.valid }">
    <label for="data" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.campaign-request.columns.data') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.data" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('data'), 'form-control-success': fields.data && fields.data.valid}" id="data" name="data" placeholder="{{ trans('admin.campaign-request.columns.data') }}">
        <div v-if="errors.has('data')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('data') }}</div>
    </div>
</div>
<div class="form-group row align-items-center" :class="{'has-danger': errors.has('campaign_id'), 'has-success': fields.campaign_id && fields.campaign_id.valid }">
    <label for="campaign_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.campaign-request.columns.campaign_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.campaign_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('campaign_id'), 'form-control-success': fields.campaign_id && fields.campaign_id.valid}" id="campaign_id" name="campaign_id" placeholder="{{ trans('admin.campaign-request.columns.campaign_id') }}">
        <div v-if="errors.has('campaign_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('campaign_id') }}</div>
    </div>
</div>
<div class="form-group row align-items-center" :class="{'has-danger': errors.has('profile_id'), 'has-success': fields.profile_id && fields.profile_id.valid }">
    <label for="profile_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">Profile id</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.profile_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('profile_id'), 'form-control-success': fields.campaign_id && fields.campaign_id.valid}" id="profile_id" name="profile_id" placeholder="Profile id">
        <div v-if="errors.has('profile_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('profile_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('payment_wallet'), 'has-success': fields.payment_wallet && fields.payment_wallet.valid }">
    <label for="payment_wallet" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.campaign-request.columns.payment_wallet') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.payment_wallet" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('payment_wallet'), 'form-control-success': fields.payment_wallet && fields.payment_wallet.valid}" id="payment_wallet" name="payment_wallet" placeholder="{{ trans('admin.campaign-request.columns.payment_wallet') }}">
        <div v-if="errors.has('payment_wallet')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('payment_wallet') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('project_id'), 'has-success': fields.project_id && fields.project_id.valid }">
    <label for="project_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.campaign-request.columns.project_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.project_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('project_id'), 'form-control-success': fields.project_id && fields.project_id.valid}" id="project_id" name="project_id" placeholder="{{ trans('admin.campaign-request.columns.project_id') }}">
        <div v-if="errors.has('project_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('project_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('status'), 'has-success': fields.status && fields.status.valid }">
    <label for="status" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.campaign-request.columns.status') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.status" v-validate="'required|integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('status'), 'form-control-success': fields.status && fields.status.valid}" id="status" name="status" placeholder="{{ trans('admin.campaign-request.columns.status') }}">
        <div v-if="errors.has('status')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('status') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('point'), 'has-success': fields.point && fields.point.valid }">
    <label for="point" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.campaign-request.columns.point') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.point" v-validate="'required|integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('point'), 'form-control-success': fields.point && fields.point.valid}" id="point" name="point" placeholder="{{ trans('admin.campaign-request.columns.point') }}">
        <div v-if="errors.has('point')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('point') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('not'), 'has-success': fields.not && fields.not.valid }">
    <label for="not" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.campaign-request.columns.not') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <textarea class="form-control" v-model="form.not" v-validate="'required'" id="not" name="not"></textarea>
        </div>
        <div v-if="errors.has('not')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('not') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('type'), 'has-success': fields.type && fields.type.valid }">
    <label for="type" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.campaign-request.columns.type') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.type" v-validate="'required|integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('type'), 'form-control-success': fields.type && fields.type.valid}" id="type" name="type" placeholder="{{ trans('admin.campaign-request.columns.type') }}">
        <div v-if="errors.has('type')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('type') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('prize'), 'has-success': fields.prize && fields.prize.valid }">
    <label for="prize" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.campaign-request.columns.prize') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.prize" v-validate="'required|integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('prize'), 'form-control-success': fields.prize && fields.prize.valid}" id="prize" name="prize" placeholder="{{ trans('admin.campaign-request.columns.prize') }}">
        <div v-if="errors.has('prize')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('prize') }}</div>
    </div>
</div>


