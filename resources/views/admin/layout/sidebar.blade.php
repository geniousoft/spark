<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">{{ trans('brackets/admin-ui::admin.sidebar.content') }}</li>
            <li class="nav-title">WEB3</li>

            <li class="nav-item"><a class="nav-link" href="{{ url('admin/')  }}"><i class="nav-icon icon-user"></i> Dashboard</a></li>

           <li class="nav-item"><a class="nav-link" href="{{ url('admin/roadmaps') }}"><i class="nav-icon icon-energy"></i> {{ trans('admin.roadmap.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/profiles') }}"><i class="nav-icon icon-magnet"></i> {{ trans('admin.profile.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/projects') }}"><i class="nav-icon icon-plane"></i> {{ trans('admin.project.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/campaigns') }}"><i class="nav-icon icon-flag"></i> {{ trans('admin.campaign.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/campaign-requests') }}"><i class="nav-icon icon-puzzle"></i> {{ trans('admin.campaign-request.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/profile-stats') }}"><i class="nav-icon icon-umbrella"></i> {{ trans('admin.profile-stat.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/campaign-stats') }}"><i class="nav-icon icon-puzzle"></i> {{ trans('admin.campaign-stat.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/nft-collections') }}"><i class="nav-icon icon-book-open"></i> {{ trans('admin.nft-collection.title') }}</a></li>
           <li class="nav-item"><a class="nav-link" href="{{ url('admin/twit-links') }}"><i class="nav-icon icon-graduation"></i> {{ trans('admin.twit-link.title') }}</a></li>
           {{-- Do not delete me :) I'm used for auto-generation menu items --}}

            <li class="nav-title">{{ trans('brackets/admin-ui::admin.sidebar.settings') }}</li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/admin-users') }}"><i class="nav-icon icon-user"></i> {{ __('Manage access') }}</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('admin/translations') }}"><i class="nav-icon icon-location-pin"></i> {{ __('Translations') }}</a></li>
            {{-- Do not delete me :) I'm also used for auto-generation menu items --}}
            {{--<li class="nav-item"><a class="nav-link" href="{{ url('admin/configuration') }}"><i class="nav-icon icon-settings"></i> {{ __('Configuration') }}</a></li>--}}
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
