@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.profile-stat.actions.edit', ['name' => $profileStat->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <profile-stat-form
                :action="'{{ $profileStat->resource_url }}'"
                :data="{{ $profileStat->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.profile-stat.actions.edit', ['name' => $profileStat->id]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.profile-stat.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </profile-stat-form>

        </div>
    
</div>

@endsection