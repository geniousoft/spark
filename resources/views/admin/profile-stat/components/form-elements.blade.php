<div class="form-group row align-items-center" :class="{'has-danger': errors.has('profile_id'), 'has-success': fields.profile_id && fields.profile_id.valid }">
    <label for="profile_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.profile-stat.columns.profile_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.profile_id" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('profile_id'), 'form-control-success': fields.profile_id && fields.profile_id.valid}" id="profile_id" name="profile_id" placeholder="{{ trans('admin.profile-stat.columns.profile_id') }}">
        <div v-if="errors.has('profile_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('profile_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('total_apply'), 'has-success': fields.total_apply && fields.total_apply.valid }">
    <label for="total_apply" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.profile-stat.columns.total_apply') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.total_apply" v-validate="'required|integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('total_apply'), 'form-control-success': fields.total_apply && fields.total_apply.valid}" id="total_apply" name="total_apply" placeholder="{{ trans('admin.profile-stat.columns.total_apply') }}">
        <div v-if="errors.has('total_apply')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('total_apply') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('total_follower'), 'has-success': fields.total_follower && fields.total_follower.valid }">
    <label for="total_follower" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.profile-stat.columns.total_follower') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.total_follower" v-validate="'required|integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('total_follower'), 'form-control-success': fields.total_follower && fields.total_follower.valid}" id="total_follower" name="total_follower" placeholder="{{ trans('admin.profile-stat.columns.total_follower') }}">
        <div v-if="errors.has('total_follower')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('total_follower') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('total_worth'), 'has-success': fields.total_worth && fields.total_worth.valid }">
    <label for="total_worth" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.profile-stat.columns.total_worth') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.total_worth" v-validate="'required|integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('total_worth'), 'form-control-success': fields.total_worth && fields.total_worth.valid}" id="total_worth" name="total_worth" placeholder="{{ trans('admin.profile-stat.columns.total_worth') }}">
        <div v-if="errors.has('total_worth')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('total_worth') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('total_point'), 'has-success': fields.total_point && fields.total_point.valid }">
    <label for="total_point" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.profile-stat.columns.total_point') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.total_point" v-validate="'required|integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('total_point'), 'form-control-success': fields.total_point && fields.total_point.valid}" id="total_point" name="total_point" placeholder="{{ trans('admin.profile-stat.columns.total_point') }}">
        <div v-if="errors.has('total_point')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('total_point') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('total_expectant'), 'has-success': fields.total_expectant && fields.total_expectant.valid }">
    <label for="total_expectant" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.profile-stat.columns.total_expectant') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.total_expectant" v-validate="'required|integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('total_expectant'), 'form-control-success': fields.total_expectant && fields.total_expectant.valid}" id="total_expectant" name="total_expectant" placeholder="{{ trans('admin.profile-stat.columns.total_expectant') }}">
        <div v-if="errors.has('total_expectant')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('total_expectant') }}</div>
    </div>
</div>


