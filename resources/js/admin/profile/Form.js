import AppForm from '../app-components/Form/AppForm';

Vue.component('profile-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                twitter_avatar:  '' ,
                twitter_handle:  '' ,
                twitter_token:  '' ,
                twitter_id:  '' ,
                system_wallet:  '' ,
                payment_wallet:  '' ,
                category:  '' ,
                follower:  '' ,
                
            }
        }
    }

});