import AppListing from '../app-components/Listing/AppListing';

Vue.component('profile-stat-listing', {
    mixins: [AppListing]
});