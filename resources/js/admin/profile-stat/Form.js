import AppForm from '../app-components/Form/AppForm';

Vue.component('profile-stat-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                project_id:  '' ,
                total_apply:  '' ,
                total_follower:  '' ,
                total_worth:  '' ,
                total_point:  '' ,
                total_expectant:  '' ,
                
            }
        }
    }

});