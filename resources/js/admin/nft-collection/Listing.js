import AppListing from '../app-components/Listing/AppListing';

Vue.component('nft-collection-listing', {
    mixins: [AppListing]
});