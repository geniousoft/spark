import AppForm from '../app-components/Form/AppForm';

Vue.component('nft-collection-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                helloMoonCollectionId:  '' ,
                image:  '' ,
                name:  '' ,
                supply:  '' ,
                
            }
        }
    }

});