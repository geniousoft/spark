import AppForm from '../app-components/Form/AppForm';

Vue.component('campaign-request-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                campaign_id:  '' ,
                data:  this.getLocalizedFormDefaults() ,
                payment_wallet:  '' ,
                project_id:  '' ,
                status:  '' ,
                point:  '' ,
                not:  '' ,
                type:  '' ,
                prize:  '' ,
                
            }
        }
    }

});