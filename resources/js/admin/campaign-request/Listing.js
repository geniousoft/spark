import AppListing from '../app-components/Listing/AppListing';

Vue.component('campaign-request-listing', {
    mixins: [AppListing]
});