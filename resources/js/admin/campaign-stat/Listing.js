import AppListing from '../app-components/Listing/AppListing';

Vue.component('campaign-stat-listing', {
    mixins: [AppListing]
});