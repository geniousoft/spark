import AppForm from '../app-components/Form/AppForm';

Vue.component('campaign-stat-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                campaign_id:  '' ,
                total_apply:  '' ,
                total_pool:  '' ,
                total_prize:  '' ,
                average_point:  '' ,
                
            }
        }
    }

});