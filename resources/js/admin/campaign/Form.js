import AppForm from '../app-components/Form/AppForm';

Vue.component('campaign-form', {
    mixins: [AppForm],
    props: [
        'projects'
    ],
    data: function() {
        return {
            form: {
                type:  '' ,
                title:  '' ,
                project_id:  '' ,
                description:  '' ,
                data:  '' ,
                per_pay:  '' ,
                code:  '' ,
                enabled:  false ,
                pubkey:  '' ,
                
            }
        }
    }

});