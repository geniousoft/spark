import AppListing from '../app-components/Listing/AppListing';

Vue.component('campaign-listing', {
    mixins: [AppListing],
    data() {
    return {
        showProjectFilter: false,
        projectMultiselect: {},

        filters: {
            project: [],
        },
    }
},

watch: {
    showProjectFilter: function (newVal, oldVal) {
        this.projectMultiselect = [];
    },
    projectMultiselect: function(newVal, oldVal) {
        this.filters.project = newVal.map(function(object) { return object['key']; });
        this.filter('project', this.filters.project);
    }
}
});