import AppListing from '../app-components/Listing/AppListing';

Vue.component('roadmap-listing', {
    mixins: [AppListing]
});