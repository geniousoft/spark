import AppForm from '../app-components/Form/AppForm';

Vue.component('roadmap-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                activated:  false ,
                date:  '' ,
                description:  '' ,
                order:  '' ,
                title:  '' ,
                
            }
        }
    }

});