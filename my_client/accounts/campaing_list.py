import typing
from dataclasses import dataclass
from construct import Construct
from solders.pubkey import Pubkey
from solana.rpc.async_api import AsyncClient
from solana.rpc.commitment import Commitment
import borsh_construct as borsh
from anchorpy.coder.accounts import ACCOUNT_DISCRIMINATOR_SIZE
from anchorpy.error import AccountInvalidDiscriminator
from anchorpy.utils.rpc import get_multiple_accounts
from anchorpy.borsh_extension import BorshPubkey
from ..program_id import PROGRAM_ID


class CampaingListJSON(typing.TypedDict):
    description: str
    url: str
    title: str
    owner: str
    vault: int
    per_pay: int
    campaing_code: str
    status: bool
    participants: list[str]


@dataclass
class CampaingList:
    discriminator: typing.ClassVar = b"\xa3\xfb\r3\xc0\x12\x04\x11"
    layout: typing.ClassVar = borsh.CStruct(
        "description" / borsh.String,
        "url" / borsh.String,
        "title" / borsh.String,
        "owner" / BorshPubkey,
        "vault" / borsh.U64,
        "per_pay" / borsh.U64,
        "campaing_code" / borsh.String,
        "status" / borsh.Bool,
        "participants" / borsh.Vec(typing.cast(Construct, BorshPubkey)),
    )
    description: str
    url: str
    title: str
    owner: Pubkey
    vault: int
    per_pay: int
    campaing_code: str
    status: bool
    participants: list[Pubkey]

    @classmethod
    async def fetch(
        cls,
        conn: AsyncClient,
        address: Pubkey,
        commitment: typing.Optional[Commitment] = None,
        program_id: Pubkey = PROGRAM_ID,
    ) -> typing.Optional["CampaingList"]:
        resp = await conn.get_account_info(address, commitment=commitment)
        info = resp.value
        if info is None:
            return None
        if info.owner != program_id:
            raise ValueError("Account does not belong to this program")
        bytes_data = info.data
        return cls.decode(bytes_data)

    @classmethod
    async def fetch_multiple(
        cls,
        conn: AsyncClient,
        addresses: list[Pubkey],
        commitment: typing.Optional[Commitment] = None,
        program_id: Pubkey = PROGRAM_ID,
    ) -> typing.List[typing.Optional["CampaingList"]]:
        infos = await get_multiple_accounts(conn, addresses, commitment=commitment)
        res: typing.List[typing.Optional["CampaingList"]] = []
        for info in infos:
            if info is None:
                res.append(None)
                continue
            if info.account.owner != program_id:
                raise ValueError("Account does not belong to this program")
            res.append(cls.decode(info.account.data))
        return res

    @classmethod
    def decode(cls, data: bytes) -> "CampaingList":
        if data[:ACCOUNT_DISCRIMINATOR_SIZE] != cls.discriminator:
            raise AccountInvalidDiscriminator(
                "The discriminator for this account is invalid"
            )
        dec = CampaingList.layout.parse(data[ACCOUNT_DISCRIMINATOR_SIZE:])
        return cls(
            description=dec.description,
            url=dec.url,
            title=dec.title,
            owner=dec.owner,
            vault=dec.vault,
            per_pay=dec.per_pay,
            campaing_code=dec.campaing_code,
            status=dec.status,
            participants=dec.participants,
        )

    def to_json(self) -> CampaingListJSON:
        return {
            "description": self.description,
            "url": self.url,
            "title": self.title,
            "owner": str(self.owner),
            "vault": self.vault,
            "per_pay": self.per_pay,
            "campaing_code": self.campaing_code,
            "status": self.status,
            "participants": list(map(lambda item: str(item), self.participants)),
        }

    @classmethod
    def from_json(cls, obj: CampaingListJSON) -> "CampaingList":
        return cls(
            description=obj["description"],
            url=obj["url"],
            title=obj["title"],
            owner=Pubkey.from_string(obj["owner"]),
            vault=obj["vault"],
            per_pay=obj["per_pay"],
            campaing_code=obj["campaing_code"],
            status=obj["status"],
            participants=list(
                map(lambda item: Pubkey.from_string(item), obj["participants"])
            ),
        )
