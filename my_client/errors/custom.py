import typing
from anchorpy.error import ProgramError


class WrongListOwner(ProgramError):
    def __init__(self) -> None:
        super().__init__(
            6000, "Specified campaing owner does not match the pubkey in the campaing"
        )

    code = 6000
    name = "WrongListOwner"
    msg = "Specified campaing owner does not match the pubkey in the campaing"


class Unauthorized(ProgramError):
    def __init__(self) -> None:
        super().__init__(6001, "Unauthorized")

    code = 6001
    name = "Unauthorized"
    msg = "Unauthorized"


class CampaingIsClose(ProgramError):
    def __init__(self) -> None:
        super().__init__(6002, "campaing is close")

    code = 6002
    name = "CampaingIsClose"
    msg = "campaing is close"


class BountyTooSmall(ProgramError):
    def __init__(self) -> None:
        super().__init__(6003, "BountyTooSmall")

    code = 6003
    name = "BountyTooSmall"
    msg = "BountyTooSmall"


class NoMoneyInCampaing(ProgramError):
    def __init__(self) -> None:
        super().__init__(6004, "No sol in left campaing")

    code = 6004
    name = "NoMoneyInCampaing"
    msg = "No sol in left campaing"


class InfB(ProgramError):
    def __init__(self) -> None:
        super().__init__(6005, "insufficient balance")

    code = 6005
    name = "InfB"
    msg = "insufficient balance"


CustomError = typing.Union[
    WrongListOwner,
    Unauthorized,
    CampaingIsClose,
    BountyTooSmall,
    NoMoneyInCampaing,
    InfB,
]
CUSTOM_ERROR_MAP: dict[int, CustomError] = {
    6000: WrongListOwner(),
    6001: Unauthorized(),
    6002: CampaingIsClose(),
    6003: BountyTooSmall(),
    6004: NoMoneyInCampaing(),
    6005: InfB(),
}


def from_code(code: int) -> typing.Optional[CustomError]:
    maybe_err = CUSTOM_ERROR_MAP.get(code)
    if maybe_err is None:
        return None
    return maybe_err
