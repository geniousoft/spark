from __future__ import annotations
import typing
from solders.pubkey import Pubkey
from solders.system_program import ID as SYS_PROGRAM_ID
from solders.instruction import Instruction, AccountMeta
import borsh_construct as borsh
from ..program_id import PROGRAM_ID


class DistrubutePlayerArgs(typing.TypedDict):
    campaing_code: str


layout = borsh.CStruct("campaing_code" / borsh.String)


class DistrubutePlayerAccounts(typing.TypedDict):
    user: Pubkey
    campaing: Pubkey
    owner: Pubkey
    player: Pubkey
    controllers: Pubkey


def distrubute_player(
    args: DistrubutePlayerArgs,
    accounts: DistrubutePlayerAccounts,
    program_id: Pubkey = PROGRAM_ID,
    remaining_accounts: typing.Optional[typing.List[AccountMeta]] = None,
) -> Instruction:
    keys: list[AccountMeta] = [
        AccountMeta(pubkey=accounts["user"], is_signer=True, is_writable=True),
        AccountMeta(pubkey=accounts["campaing"], is_signer=False, is_writable=True),
        AccountMeta(pubkey=accounts["owner"], is_signer=False, is_writable=True),
        AccountMeta(pubkey=accounts["player"], is_signer=False, is_writable=True),
        AccountMeta(pubkey=accounts["controllers"], is_signer=False, is_writable=True),
        AccountMeta(pubkey=SYS_PROGRAM_ID, is_signer=False, is_writable=False),
    ]
    if remaining_accounts is not None:
        keys += remaining_accounts
    identifier = b"\x9a?\xe0\xa7j\xf9\xbc8"
    encoded_args = layout.build(
        {
            "campaing_code": args["campaing_code"],
        }
    )
    data = identifier + encoded_args
    return Instruction(program_id, data, keys)
