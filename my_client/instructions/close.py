from __future__ import annotations
import typing
from solders.pubkey import Pubkey
from solders.instruction import Instruction, AccountMeta
import borsh_construct as borsh
from ..program_id import PROGRAM_ID


class CloseArgs(typing.TypedDict):
    campaing_code: str


layout = borsh.CStruct("campaing_code" / borsh.String)


class CloseAccounts(typing.TypedDict):
    campaing: Pubkey
    owner: Pubkey
    user: Pubkey


def close(
    args: CloseArgs,
    accounts: CloseAccounts,
    program_id: Pubkey = PROGRAM_ID,
    remaining_accounts: typing.Optional[typing.List[AccountMeta]] = None,
) -> Instruction:
    keys: list[AccountMeta] = [
        AccountMeta(pubkey=accounts["campaing"], is_signer=False, is_writable=True),
        AccountMeta(pubkey=accounts["owner"], is_signer=False, is_writable=False),
        AccountMeta(pubkey=accounts["user"], is_signer=True, is_writable=False),
    ]
    if remaining_accounts is not None:
        keys += remaining_accounts
    identifier = b"b\xa5\xc9\xb1lA\xce`"
    encoded_args = layout.build(
        {
            "campaing_code": args["campaing_code"],
        }
    )
    data = identifier + encoded_args
    return Instruction(program_id, data, keys)
