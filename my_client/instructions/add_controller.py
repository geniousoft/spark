from __future__ import annotations
import typing
from solders.pubkey import Pubkey
from solders.system_program import ID as SYS_PROGRAM_ID
from solders.instruction import Instruction, AccountMeta
from anchorpy.borsh_extension import BorshPubkey
import borsh_construct as borsh
from ..program_id import PROGRAM_ID


class AddControllerArgs(typing.TypedDict):
    key: Pubkey


layout = borsh.CStruct("key" / BorshPubkey)


class AddControllerAccounts(typing.TypedDict):
    signer: Pubkey
    controllers: Pubkey
    program: Pubkey
    program_data: Pubkey


def add_controller(
    args: AddControllerArgs,
    accounts: AddControllerAccounts,
    program_id: Pubkey = PROGRAM_ID,
    remaining_accounts: typing.Optional[typing.List[AccountMeta]] = None,
) -> Instruction:
    keys: list[AccountMeta] = [
        AccountMeta(pubkey=accounts["signer"], is_signer=True, is_writable=True),
        AccountMeta(pubkey=accounts["controllers"], is_signer=False, is_writable=True),
        AccountMeta(pubkey=accounts["program"], is_signer=False, is_writable=False),
        AccountMeta(
            pubkey=accounts["program_data"], is_signer=False, is_writable=False
        ),
        AccountMeta(pubkey=SYS_PROGRAM_ID, is_signer=False, is_writable=False),
    ]
    if remaining_accounts is not None:
        keys += remaining_accounts
    identifier = b"\xc5E\xa3\x1a\r0u\xef"
    encoded_args = layout.build(
        {
            "key": args["key"],
        }
    )
    data = identifier + encoded_args
    return Instruction(program_id, data, keys)
