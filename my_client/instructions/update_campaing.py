from __future__ import annotations
import typing
from solders.pubkey import Pubkey
from solders.instruction import Instruction, AccountMeta
import borsh_construct as borsh
from ..program_id import PROGRAM_ID


class UpdateCampaingArgs(typing.TypedDict):
    campaing_code: str
    description: str
    url: str
    title: str


layout = borsh.CStruct(
    "campaing_code" / borsh.String,
    "description" / borsh.String,
    "url" / borsh.String,
    "title" / borsh.String,
)


class UpdateCampaingAccounts(typing.TypedDict):
    campaing: Pubkey
    owner: Pubkey
    user: Pubkey


def update_campaing(
    args: UpdateCampaingArgs,
    accounts: UpdateCampaingAccounts,
    program_id: Pubkey = PROGRAM_ID,
    remaining_accounts: typing.Optional[typing.List[AccountMeta]] = None,
) -> Instruction:
    keys: list[AccountMeta] = [
        AccountMeta(pubkey=accounts["campaing"], is_signer=False, is_writable=True),
        AccountMeta(pubkey=accounts["owner"], is_signer=False, is_writable=False),
        AccountMeta(pubkey=accounts["user"], is_signer=True, is_writable=False),
    ]
    if remaining_accounts is not None:
        keys += remaining_accounts
    identifier = b"6\xdb\xb0d\xe1\xb5\x043"
    encoded_args = layout.build(
        {
            "campaing_code": args["campaing_code"],
            "description": args["description"],
            "url": args["url"],
            "title": args["title"],
        }
    )
    data = identifier + encoded_args
    return Instruction(program_id, data, keys)
