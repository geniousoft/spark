from __future__ import annotations
import typing
from solders.pubkey import Pubkey
from solders.system_program import ID as SYS_PROGRAM_ID
from solders.instruction import Instruction, AccountMeta
import borsh_construct as borsh
from ..program_id import PROGRAM_ID


class CampaingCreateArgs(typing.TypedDict):
    campaing_code: str
    description: str
    url: str
    title: str
    per_pay: int


layout = borsh.CStruct(
    "campaing_code" / borsh.String,
    "description" / borsh.String,
    "url" / borsh.String,
    "title" / borsh.String,
    "per_pay" / borsh.U64,
)


class CampaingCreateAccounts(typing.TypedDict):
    owner: Pubkey
    campaing: Pubkey


def campaing_create(
    args: CampaingCreateArgs,
    accounts: CampaingCreateAccounts,
    program_id: Pubkey = PROGRAM_ID,
    remaining_accounts: typing.Optional[typing.List[AccountMeta]] = None,
) -> Instruction:
    keys: list[AccountMeta] = [
        AccountMeta(pubkey=accounts["owner"], is_signer=True, is_writable=True),
        AccountMeta(pubkey=accounts["campaing"], is_signer=False, is_writable=True),
        AccountMeta(pubkey=SYS_PROGRAM_ID, is_signer=False, is_writable=False),
    ]
    if remaining_accounts is not None:
        keys += remaining_accounts
    identifier = b"\x86\xfa\n\x94#g\x00\xc4"
    encoded_args = layout.build(
        {
            "campaing_code": args["campaing_code"],
            "description": args["description"],
            "url": args["url"],
            "title": args["title"],
            "per_pay": args["per_pay"],
        }
    )
    data = identifier + encoded_args
    return Instruction(program_id, data, keys)
