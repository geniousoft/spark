from __future__ import annotations
import typing
from solders.pubkey import Pubkey
from solders.system_program import ID as SYS_PROGRAM_ID
from solders.instruction import Instruction, AccountMeta
import borsh_construct as borsh
from ..program_id import PROGRAM_ID


class DepositeArgs(typing.TypedDict):
    campaing_code: str
    amount: int


layout = borsh.CStruct("campaing_code" / borsh.String, "amount" / borsh.U64)


class DepositeAccounts(typing.TypedDict):
    user: Pubkey
    campaing: Pubkey
    owner: Pubkey


def deposite(
    args: DepositeArgs,
    accounts: DepositeAccounts,
    program_id: Pubkey = PROGRAM_ID,
    remaining_accounts: typing.Optional[typing.List[AccountMeta]] = None,
) -> Instruction:
    keys: list[AccountMeta] = [
        AccountMeta(pubkey=accounts["user"], is_signer=True, is_writable=True),
        AccountMeta(pubkey=accounts["campaing"], is_signer=False, is_writable=True),
        AccountMeta(pubkey=accounts["owner"], is_signer=False, is_writable=False),
        AccountMeta(pubkey=SYS_PROGRAM_ID, is_signer=False, is_writable=False),
    ]
    if remaining_accounts is not None:
        keys += remaining_accounts
    identifier = b"v\xd0\x95\xd9\xf7\xdd\x17k"
    encoded_args = layout.build(
        {
            "campaing_code": args["campaing_code"],
            "amount": args["amount"],
        }
    )
    data = identifier + encoded_args
    return Instruction(program_id, data, keys)
