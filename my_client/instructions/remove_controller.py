from __future__ import annotations
import typing
from solders.pubkey import Pubkey
from solders.system_program import ID as SYS_PROGRAM_ID
from solders.instruction import Instruction, AccountMeta
from anchorpy.borsh_extension import BorshPubkey
import borsh_construct as borsh
from ..program_id import PROGRAM_ID


class RemoveControllerArgs(typing.TypedDict):
    key: Pubkey


layout = borsh.CStruct("key" / BorshPubkey)


class RemoveControllerAccounts(typing.TypedDict):
    signer: Pubkey
    controllers: Pubkey
    program: Pubkey
    program_data: Pubkey


def remove_controller(
    args: RemoveControllerArgs,
    accounts: RemoveControllerAccounts,
    program_id: Pubkey = PROGRAM_ID,
    remaining_accounts: typing.Optional[typing.List[AccountMeta]] = None,
) -> Instruction:
    keys: list[AccountMeta] = [
        AccountMeta(pubkey=accounts["signer"], is_signer=True, is_writable=True),
        AccountMeta(pubkey=accounts["controllers"], is_signer=False, is_writable=True),
        AccountMeta(pubkey=accounts["program"], is_signer=False, is_writable=False),
        AccountMeta(
            pubkey=accounts["program_data"], is_signer=False, is_writable=False
        ),
        AccountMeta(pubkey=SYS_PROGRAM_ID, is_signer=False, is_writable=False),
    ]
    if remaining_accounts is not None:
        keys += remaining_accounts
    identifier = b"\xf8?\xdcW\x85\xac=o"
    encoded_args = layout.build(
        {
            "key": args["key"],
        }
    )
    data = identifier + encoded_args
    return Instruction(program_id, data, keys)
