<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\DashboardStats;
use App\Models\Roadmap;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DashboardStats::create([
            'total_profiles' => 0,
            'total_requests' => 0,
            'total_campaigns' => 0,
            'total_active_campaigns' => 0,
        ]);

        Roadmap::create([
            'activated'=>1,
            'date'=>'Q1 2023',
            'description' => 'Installing Spark online in test environment via devnet on development. In this process, the tests on devnet are completed and the system is made ready with 100% working security.',
            'order'=>1,
            'title'=>'Deploy',
        ]);
        Roadmap::create([
            'activated'=>1,
            'date'=>'Q2 2023',
            'description' => 'Activation of the system prepared on Spark online. Self-use as marketing in this process. Increasing awareness through digital advertisements.',
            'order'=>2,
            'title'=>'Marketing',
        ]);

    }
}
