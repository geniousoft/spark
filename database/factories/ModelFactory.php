<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Brackets\AdminAuth\Models\AdminUser::class, function (Faker\Generator $faker) {
    return [
        'activated' => true,
        'created_at' => $faker->dateTime,
        'deleted_at' => null,
        'email' => $faker->email,
        'first_name' => $faker->firstName,
        'forbidden' => $faker->boolean(),
        'language' => 'en',
        'last_login_at' => $faker->dateTime,
        'last_name' => $faker->lastName,
        'password' => bcrypt($faker->password),
        'remember_token' => null,
        'updated_at' => $faker->dateTime,
        
    ];
});/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\RoadMap::class, static function (Faker\Generator $faker) {
    return [
        
        
    ];
});
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Roadmap::class, static function (Faker\Generator $faker) {
    return [
        
        
    ];
});
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Roadmap::class, static function (Faker\Generator $faker) {
    return [
        'activated' => $faker->boolean(),
        'created_at' => $faker->dateTime,
        'date' => $faker->sentence,
        'deleted_at' => null,
        'description' => $faker->text(),
        'order' => $faker->randomNumber(5),
        'title' => $faker->sentence,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Profile::class, static function (Faker\Generator $faker) {
    return [
        'twitter_avatar' => $faker->sentence,
        'twitter_handle' => $faker->sentence,
        'twitter_token' => $faker->sentence,
        'twitter_id' => $faker->randomNumber(5),
        'system_wallet' => $faker->sentence,
        'payment_wallet' => $faker->sentence,
        'category' => $faker->randomNumber(5),
        'follower' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Project::class, static function (Faker\Generator $faker) {
    return [
        'twitter' => $faker->sentence,
        'discord' => $faker->sentence,
        'link' => $faker->sentence,
        'name' => $faker->firstName,
        'description' => $faker->sentence,
        'logo' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Campaign::class, static function (Faker\Generator $faker) {
    return [
        'type' => $faker->randomNumber(5),
        'title' => $faker->sentence,
        'project_id' => $faker->sentence,
        'description' => $faker->text(),
        'data' => $faker->text(),
        'per_pay' => $faker->randomNumber(5),
        'code' => $faker->randomNumber(5),
        'enabled' => $faker->boolean(),
        'pubkey' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\CampaignRequest::class, static function (Faker\Generator $faker) {
    return [
        'campaign_id' => $faker->sentence,
        'payment_wallet' => $faker->sentence,
        'project_id' => $faker->sentence,
        'status' => $faker->randomNumber(5),
        'point' => $faker->randomNumber(5),
        'not' => $faker->text(),
        'type' => $faker->randomNumber(5),
        'prize' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        'data' => ['en' => $faker->sentence],
        
    ];
});
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\ProfileStat::class, static function (Faker\Generator $faker) {
    return [
        'project_id' => $faker->sentence,
        'total_apply' => $faker->randomNumber(5),
        'total_follower' => $faker->randomNumber(5),
        'total_worth' => $faker->randomNumber(5),
        'total_point' => $faker->randomNumber(5),
        'total_expectant' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\CampaignStat::class, static function (Faker\Generator $faker) {
    return [
        'campaign_id' => $faker->sentence,
        'total_apply' => $faker->randomNumber(5),
        'total_pool' => $faker->randomNumber(5),
        'total_prize' => $faker->randomNumber(5),
        'average_point' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\NFTCollection::class, static function (Faker\Generator $faker) {
    return [
        'created_at' => $faker->dateTime,
        'helloMoonCollectionId' => $faker->sentence,
        'image' => $faker->sentence,
        'name' => $faker->firstName,
        'supply' => $faker->randomNumber(5),
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\NftCollection::class, static function (Faker\Generator $faker) {
    return [
        'created_at' => $faker->dateTime,
        'helloMoonCollectionId' => $faker->sentence,
        'image' => $faker->sentence,
        'name' => $faker->firstName,
        'supply' => $faker->randomNumber(5),
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\TwitLink::class, static function (Faker\Generator $faker) {
    return [
        'created_at' => $faker->dateTime,
        'twit_id' => $faker->sentence,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
