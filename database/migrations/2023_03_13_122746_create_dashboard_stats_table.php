<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dashboard_stats', function (Blueprint $table) {
            $table->id();
            $table->float('total_profiles')->nullable();
            $table->float('total_requests')->nullable();
            $table->float('total_campaigns')->nullable();
            $table->float('total_active_campaigns')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dashboard_stats');
    }
};
