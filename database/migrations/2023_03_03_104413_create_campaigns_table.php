<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->id();
            $table->integer("type");
            $table->string("title");
            $table->string("system_wallet");
            $table->unsignedBigInteger('project_id')->nullable();
            $table->text("description");
            $table->text("data");
            $table->integer("per_pay");
            $table->string("code");
            $table->boolean("enabled")->default(0);
            $table->string("pubkey")->nullable();
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
};
