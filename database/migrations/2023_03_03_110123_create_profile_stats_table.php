<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_stats', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("profile_id");
            $table->integer('total_apply')->default(0);
            $table->integer('total_follower')->default(0);
            $table->float('total_worth')->default(0);
            $table->integer('total_point')->default(0);
            $table->float('total_expectant')->default(0);
            $table->timestamps();
            $table->foreign('profile_id')->references('id')->on('profiles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_stats');
    }
};
