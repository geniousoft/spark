<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->id();
            $table->string('twitter_avatar');
            $table->string('twitter_handle');
            $table->string('twitter_token');
            $table->bigInteger('twitter_id')->nullable();
            $table->string('system_wallet')->nullable();
            $table->string('payment_wallet')->nullable();

            $table->integer('category')->nullable();
            // arkaplanda okunucak
            $table->integer('follower')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
};
