<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\NftCollection\BulkDestroyNftCollection;
use App\Http\Requests\Admin\NftCollection\DestroyNftCollection;
use App\Http\Requests\Admin\NftCollection\IndexNftCollection;
use App\Http\Requests\Admin\NftCollection\StoreNftCollection;
use App\Http\Requests\Admin\NftCollection\UpdateNftCollection;
use App\Models\NftCollection;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class NftCollectionsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexNftCollection $request
     * @return array|Factory|View
     */
    public function index(IndexNftCollection $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(NftCollection::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['helloMoonCollectionId', 'id', 'image', 'name', 'supply'],

            // set columns to searchIn
            ['helloMoonCollectionId', 'id', 'image', 'name']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.nft-collection.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.nft-collection.create');

        return view('admin.nft-collection.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreNftCollection $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreNftCollection $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the NftCollection
        $nftCollection = NftCollection::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/nft-collections'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/nft-collections');
    }

    /**
     * Display the specified resource.
     *
     * @param NftCollection $nftCollection
     * @throws AuthorizationException
     * @return void
     */
    public function show(NftCollection $nftCollection)
    {
        $this->authorize('admin.nft-collection.show', $nftCollection);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param NftCollection $nftCollection
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(NftCollection $nftCollection)
    {
        $this->authorize('admin.nft-collection.edit', $nftCollection);


        return view('admin.nft-collection.edit', [
            'nftCollection' => $nftCollection,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateNftCollection $request
     * @param NftCollection $nftCollection
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateNftCollection $request, NftCollection $nftCollection)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values NftCollection
        $nftCollection->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/nft-collections'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/nft-collections');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyNftCollection $request
     * @param NftCollection $nftCollection
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyNftCollection $request, NftCollection $nftCollection)
    {
        $nftCollection->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyNftCollection $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyNftCollection $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    NftCollection::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
