<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CampaignRequest\BulkDestroyCampaignRequest;
use App\Http\Requests\Admin\CampaignRequest\DestroyCampaignRequest;
use App\Http\Requests\Admin\CampaignRequest\IndexCampaignRequest;
use App\Http\Requests\Admin\CampaignRequest\StoreCampaignRequest;
use App\Http\Requests\Admin\CampaignRequest\UpdateCampaignRequest;
use App\Models\CampaignRequest;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class CampaignRequestsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexCampaignRequest $request
     * @return array|Factory|View
     */
    public function index(IndexCampaignRequest $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(CampaignRequest::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'campaign_id', 'data', 'payment_wallet', 'profile_id', 'status', 'point', 'type', 'prize'],

            // set columns to searchIn
            ['id', 'data', 'payment_wallet', 'not']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.campaign-request.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.campaign-request.create');

        return view('admin.campaign-request.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCampaignRequest $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreCampaignRequest $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the CampaignRequest
        $campaignRequest = CampaignRequest::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/campaign-requests'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/campaign-requests');
    }

    /**
     * Display the specified resource.
     *
     * @param CampaignRequest $campaignRequest
     * @throws AuthorizationException
     * @return void
     */
    public function show(CampaignRequest $campaignRequest)
    {
        $this->authorize('admin.campaign-request.show', $campaignRequest);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param CampaignRequest $campaignRequest
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(CampaignRequest $campaignRequest)
    {
        $this->authorize('admin.campaign-request.edit', $campaignRequest);


        return view('admin.campaign-request.edit', [
            'campaignRequest' => $campaignRequest,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCampaignRequest $request
     * @param CampaignRequest $campaignRequest
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateCampaignRequest $request, CampaignRequest $campaignRequest)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values CampaignRequest
        $campaignRequest->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/campaign-requests'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/campaign-requests');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyCampaignRequest $request
     * @param CampaignRequest $campaignRequest
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyCampaignRequest $request, CampaignRequest $campaignRequest)
    {
        $campaignRequest->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyCampaignRequest $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyCampaignRequest $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    CampaignRequest::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
