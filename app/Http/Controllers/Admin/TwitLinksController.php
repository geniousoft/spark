<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TwitLink\BulkDestroyTwitLink;
use App\Http\Requests\Admin\TwitLink\DestroyTwitLink;
use App\Http\Requests\Admin\TwitLink\IndexTwitLink;
use App\Http\Requests\Admin\TwitLink\StoreTwitLink;
use App\Http\Requests\Admin\TwitLink\UpdateTwitLink;
use App\Models\TwitLink;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class TwitLinksController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexTwitLink $request
     * @return array|Factory|View
     */
    public function index(IndexTwitLink $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(TwitLink::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'twit_id'],

            // set columns to searchIn
            ['id']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.twit-link.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.twit-link.create');

        return view('admin.twit-link.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTwitLink $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreTwitLink $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the TwitLink
        $twitLink = TwitLink::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/twit-links'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/twit-links');
    }

    /**
     * Display the specified resource.
     *
     * @param TwitLink $twitLink
     * @throws AuthorizationException
     * @return void
     */
    public function show(TwitLink $twitLink)
    {
        $this->authorize('admin.twit-link.show', $twitLink);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param TwitLink $twitLink
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(TwitLink $twitLink)
    {
        $this->authorize('admin.twit-link.edit', $twitLink);


        return view('admin.twit-link.edit', [
            'twitLink' => $twitLink,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateTwitLink $request
     * @param TwitLink $twitLink
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateTwitLink $request, TwitLink $twitLink)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values TwitLink
        $twitLink->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/twit-links'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/twit-links');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyTwitLink $request
     * @param TwitLink $twitLink
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyTwitLink $request, TwitLink $twitLink)
    {
        $twitLink->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyTwitLink $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyTwitLink $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    TwitLink::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
