<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CampaignStat\BulkDestroyCampaignStat;
use App\Http\Requests\Admin\CampaignStat\DestroyCampaignStat;
use App\Http\Requests\Admin\CampaignStat\IndexCampaignStat;
use App\Http\Requests\Admin\CampaignStat\StoreCampaignStat;
use App\Http\Requests\Admin\CampaignStat\UpdateCampaignStat;
use App\Models\CampaignStat;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class CampaignStatsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexCampaignStat $request
     * @return array|Factory|View
     */
    public function index(IndexCampaignStat $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(CampaignStat::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'campaign_id', 'total_apply', 'total_pool', 'total_prize', 'average_point'],

            // set columns to searchIn
            ['id']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.campaign-stat.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.campaign-stat.create');

        return view('admin.campaign-stat.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCampaignStat $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreCampaignStat $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the CampaignStat
        $campaignStat = CampaignStat::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/campaign-stats'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/campaign-stats');
    }

    /**
     * Display the specified resource.
     *
     * @param CampaignStat $campaignStat
     * @throws AuthorizationException
     * @return void
     */
    public function show(CampaignStat $campaignStat)
    {
        $this->authorize('admin.campaign-stat.show', $campaignStat);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param CampaignStat $campaignStat
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(CampaignStat $campaignStat)
    {
        $this->authorize('admin.campaign-stat.edit', $campaignStat);


        return view('admin.campaign-stat.edit', [
            'campaignStat' => $campaignStat,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCampaignStat $request
     * @param CampaignStat $campaignStat
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateCampaignStat $request, CampaignStat $campaignStat)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values CampaignStat
        $campaignStat->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/campaign-stats'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/campaign-stats');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyCampaignStat $request
     * @param CampaignStat $campaignStat
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyCampaignStat $request, CampaignStat $campaignStat)
    {
        $campaignStat->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyCampaignStat $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyCampaignStat $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    CampaignStat::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
