<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Roadmap\BulkDestroyRoadmap;
use App\Http\Requests\Admin\Roadmap\DestroyRoadmap;
use App\Http\Requests\Admin\Roadmap\IndexRoadmap;
use App\Http\Requests\Admin\Roadmap\StoreRoadmap;
use App\Http\Requests\Admin\Roadmap\UpdateRoadmap;
use App\Models\Roadmap;
use Brackets\AdminListing\Facades\AdminListing;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class RoadmapController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexRoadmap $request
     * @return array|Factory|View
     */
    public function index(IndexRoadmap $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Roadmap::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['activated', 'date', 'id', 'order', 'title'],

            // set columns to searchIn
            ['date', 'description', 'id', 'title']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.roadmap.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.roadmap.create');

        return view('admin.roadmap.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRoadmap $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreRoadmap $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the Roadmap
        $roadmap = Roadmap::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/roadmaps'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/roadmaps');
    }

    /**
     * Display the specified resource.
     *
     * @param Roadmap $roadmap
     * @throws AuthorizationException
     * @return void
     */
    public function show(Roadmap $roadmap)
    {
        $this->authorize('admin.roadmap.show', $roadmap);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Roadmap $roadmap
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Roadmap $roadmap)
    {
        $this->authorize('admin.roadmap.edit', $roadmap);


        return view('admin.roadmap.edit', [
            'roadmap' => $roadmap,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRoadmap $request
     * @param Roadmap $roadmap
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateRoadmap $request, Roadmap $roadmap)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values Roadmap
        $roadmap->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/roadmaps'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/roadmaps');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyRoadmap $request
     * @param Roadmap $roadmap
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyRoadmap $request, Roadmap $roadmap)
    {
        $roadmap->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyRoadmap $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyRoadmap $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    DB::table('roadmaps')->whereIn('id', $bulkChunk)
                        ->update([
                            'deleted_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
