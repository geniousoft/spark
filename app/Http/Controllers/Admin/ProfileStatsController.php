<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProfileStat\BulkDestroyProfileStat;
use App\Http\Requests\Admin\ProfileStat\DestroyProfileStat;
use App\Http\Requests\Admin\ProfileStat\IndexProfileStat;
use App\Http\Requests\Admin\ProfileStat\StoreProfileStat;
use App\Http\Requests\Admin\ProfileStat\UpdateProfileStat;
use App\Models\ProfileStat;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ProfileStatsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexProfileStat $request
     * @return array|Factory|View
     */
    public function index(IndexProfileStat $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(ProfileStat::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'profile_id', 'total_apply', 'total_follower', 'total_worth', 'total_point', 'total_expectant'],

            // set columns to searchIn
            ['id']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.profile-stat.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.profile-stat.create');

        return view('admin.profile-stat.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreProfileStat $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreProfileStat $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the ProfileStat
        $profileStat = ProfileStat::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/profile-stats'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/profile-stats');
    }

    /**
     * Display the specified resource.
     *
     * @param ProfileStat $profileStat
     * @throws AuthorizationException
     * @return void
     */
    public function show(ProfileStat $profileStat)
    {
        $this->authorize('admin.profile-stat.show', $profileStat);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ProfileStat $profileStat
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(ProfileStat $profileStat)
    {
        $this->authorize('admin.profile-stat.edit', $profileStat);


        return view('admin.profile-stat.edit', [
            'profileStat' => $profileStat,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProfileStat $request
     * @param ProfileStat $profileStat
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateProfileStat $request, ProfileStat $profileStat)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values ProfileStat
        $profileStat->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/profile-stats'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/profile-stats');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyProfileStat $request
     * @param ProfileStat $profileStat
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyProfileStat $request, ProfileStat $profileStat)
    {
        $profileStat->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyProfileStat $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyProfileStat $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    ProfileStat::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
