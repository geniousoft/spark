<?php

namespace App\Http\Controllers;

use App\Models\ProfileSettings;
use Illuminate\Http\Request;

class ProfileSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProfileSettings  $profileSettings
     * @return \Illuminate\Http\Response
     */
    public function show(ProfileSettings $profileSettings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProfileSettings  $profileSettings
     * @return \Illuminate\Http\Response
     */
    public function edit(ProfileSettings $profileSettings)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProfileSettings  $profileSettings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProfileSettings $profileSettings)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProfileSettings  $profileSettings
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProfileSettings $profileSettings)
    {
        //
    }
}
