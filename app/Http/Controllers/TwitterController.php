<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\Request;

class TwitterController extends Controller
{
    public function redirect(Request $request)
    {
        $data =explode("SPARKABO",$request->message);
        $sing = rawurldecode($data[0]);
        $wallet = rawurldecode($data[1]);
        $dwallet =  sodium_base642bin($wallet,SODIUM_BASE64_VARIANT_ORIGINAL);
        $bin = sodium_base642bin($sing,SODIUM_BASE64_VARIANT_ORIGINAL);
        $d = sodium_crypto_sign_verify_detached($bin, "Welcome to spark",  $dwallet);
        $bitcoin = new \Tuupola\Base58(["characters" => \Tuupola\Base58::BITCOIN]);
        $finalwallet = $bitcoin->encode(base64_decode($wallet));
        if($d){
            session()->put('trusted_wallet', $finalwallet);
            return Socialite::driver('twitter')->redirect();
        }else{
            abort(403);
        }
    }
    public function callback()
    {
        $profile = Socialite::driver('twitter')->user();
        $data = [
            'twitter_avatar' => $profile->avatar,
            'twitter_handle' => $profile->nickname,
            'twitter_id' => $profile->id,
            'twitter_token' => $profile->token,
        ];
        if(session()->has('trusted_wallet')){
            $wallet = session()->get("trusted_wallet");
            $data['system_wallet'] =    $wallet;
        }
        session()->forget('trusted_wallet');
        Profile::updateOrCreate(['twitter_id' => $profile->id], $data);
        session()->put('profile_id', $profile->id);
        return redirect()->to("https://dashboard.sparkonline.xyz");
    }
    public function save(Request $request)
    {
        $id = session()->get('profile_id');
        $profile = Profile::where('twitter_id', $id)->firstOrFail();
        $profile->update([
            'system_wallet' => $request->system_wallet,
            'payment_wallet' => $request->payment_wallet,
            'category' => $request->category,
        ]);
        session()->put('message', "Successfull updated");
        return redirect()->to("https://dashboard.sparkonline.xyz");
    }
    public function logout()
    {
        session()->forget("profile_id");
        return redirect()->to("https://dashboard.sparkonline.xyz");
    }
   
}
