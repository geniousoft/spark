<?php

namespace App\Http\Requests\Admin\ProfileStat;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateProfileStat extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.profile-stat.edit', $this->profileStat);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'profile_id' => ['sometimes', 'string'],
            'total_apply' => ['sometimes', 'integer'],
            'total_follower' => ['sometimes', 'integer'],
            'total_worth' => ['sometimes', 'integer'],
            'total_point' => ['sometimes', 'integer'],
            'total_expectant' => ['sometimes', 'integer'],
            
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
