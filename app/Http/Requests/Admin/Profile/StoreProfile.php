<?php

namespace App\Http\Requests\Admin\Profile;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.profile.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'twitter_avatar' => ['required', 'string'],
            'twitter_handle' => ['required', 'string'],
            'twitter_token' => ['required', 'string'],
            'twitter_id' => ['nullable', 'integer'],
            'system_wallet' => ['nullable', 'string'],
            'payment_wallet' => ['nullable', 'string'],
            'category' => ['nullable', 'integer'],
            'follower' => ['nullable', 'integer'],
            
        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
