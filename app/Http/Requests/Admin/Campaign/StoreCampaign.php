<?php

namespace App\Http\Requests\Admin\Campaign;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreCampaign extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.campaign.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'type' => ['required', 'integer'],
            'title' => ['required', 'string'],
            'project' => ['nullable'],
            'description' => ['required', 'string'],
            'data' => ['required', 'string'],
            'per_pay' => ['required', 'integer'],
            'code' => ['required'],
            'enabled' => ['required', 'boolean'],
            'pubkey' => ['nullable', 'string'],
            
        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }
    public function getProjectId(){
        if ($this->has('project')){
            return $this->get('project')['id'];
        }
        return null;
    }
}
