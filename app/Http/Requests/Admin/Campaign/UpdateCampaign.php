<?php

namespace App\Http\Requests\Admin\Campaign;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateCampaign extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.campaign.edit', $this->campaign);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'type' => ['sometimes', 'integer'],
            'title' => ['sometimes', 'string'],
            'project' => ['nullable'],
            'description' => ['sometimes', 'string'],
            'data' => ['sometimes', 'string'],
            'per_pay' => ['sometimes', 'integer'],
            'code' => ['sometimes'],
            'enabled' => ['sometimes', 'boolean'],
            'pubkey' => ['nullable', 'string'],
            
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
    public function getProjectId(){
        if ($this->has('project')){
            return $this->get('project')['id'];
        }
        return null;
    }
}
