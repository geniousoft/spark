<?php

namespace App\Http\Requests\Admin\CampaignRequest;

use Brackets\Translatable\TranslatableFormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateCampaignRequest extends TranslatableFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.campaign-request.edit', $this->campaignRequest);
    }

/**
     * Get the validation rules that apply to the requests untranslatable fields.
     *
     * @return array
     */
    public function untranslatableRules(): array {
        return [
            'campaign_id' => ['sometimes'],
            'payment_wallet' => ['sometimes', 'string'],
            'project_id' => ['sometimes'],
            'status' => ['sometimes', 'integer'],
            'point' => ['sometimes', 'integer'],
            'not' => ['sometimes', 'string'],
            'type' => ['sometimes', 'integer'],
            'prize' => ['sometimes', 'integer'],
            

        ];
    }

    /**
     * Get the validation rules that apply to the requests translatable fields.
     *
     * @return array
     */
    public function translatableRules($locale): array {
        return [
            'data' => ['sometimes', 'string'],
            
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
