<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Spatie\Health\Checks\Checks\CacheCheck;
use Spatie\Health\Checks\Checks\DebugModeCheck;
use Spatie\Health\Facades\Health;
use Spatie\Health\Checks\Checks\DatabaseCheck;
use Spatie\Health\Checks\Checks\EnvironmentCheck;
use Spatie\Health\Checks\Checks\PingCheck;
use Spatie\Health\Checks\Checks\UsedDiskSpaceCheck;

class HealthServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        Health::checks([
            DebugModeCheck::new(),
            CacheCheck::new(),
            DatabaseCheck::new(),
            UsedDiskSpaceCheck::new(),
            PingCheck::new()->name('api.sparkonline.xyz')->url('https://api.sparkonline.xyz'),
            PingCheck::new()->name('dashboard.sparkonline.xyz')->url('https://dashboard.sparkonline.xyz'),
            EnvironmentCheck::new(),


        ]);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
