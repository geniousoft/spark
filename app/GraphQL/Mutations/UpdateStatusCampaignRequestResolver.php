<?php

namespace App\GraphQL\Mutations;

use App\Models\Campaign;
use App\Models\CampaignRequest;
use Exception;

final class UpdateStatusCampaignRequestResolver
{
    /**
     * @param  null  $_
     * @param  array{}  $args
     */
    public function __invoke($_, array $args)
    {
        $reqID = CampaignRequest::where('id',$args['id'])->first();
        if($reqID->type != Campaign::MICRO){
            throw new Exception("Error Only Micro Campaign", 1);
        }
     
        if($reqID->status != CampaignRequest::PENDING){
            throw new Exception("Error Requests on Pending", 1);
        }
        if($args['status'] == CampaignRequest::APPROVED || $args['status'] == CampaignRequest::REJECTED){
            $reqID->update(['status' => $args['status']]);
        }else{
            throw new Exception("Error Only Approved or Rejected Campaign Requests", 1);
        }
    }
}
