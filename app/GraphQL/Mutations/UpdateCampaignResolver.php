<?php

namespace App\GraphQL\Mutations;


use App\Models\Campaign;

final class UpdateCampaignResolver
{
    /**
     * @param  null  $_
     * @param  array{}  $args
     */
    public function __invoke($_, array $args)
    {
        $campaing = Campaign::where('code',$args['code'])->firstOrFail();
        $campaing->update([
            'pubkey' => $args['pubkey'],
            'enabled'=>1
        ]);
        return $campaing;
    }
}
