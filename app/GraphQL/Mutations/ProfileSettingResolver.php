<?php

namespace App\GraphQL\Mutations;

use App\Models\Profile;
use App\Models\ProfileSettings;
use Exception;

final class ProfileSettingResolver
{
    /**
     * @param  null  $_
     * @param  array{}  $args
     */
    public function __invoke($_, array $args)
    {
        $data = explode("SPARKPROFILEUPDATE", $args['val']);
        $sing = rawurldecode($data[0]);
        $wallet = rawurldecode($data[1]);
        $dwallet =  sodium_base642bin($wallet, SODIUM_BASE64_VARIANT_ORIGINAL);
        $bin = sodium_base642bin($sing, SODIUM_BASE64_VARIANT_ORIGINAL);
        $d = sodium_crypto_sign_verify_detached($bin, $args['email'],  $dwallet);
        if (!$d) {
            throw new Exception("Not match sign and request", 1);
        }
        $profile = Profile::where('system_wallet', $args['system_wallet'])->first();
        ProfileSettings::updateOrCreate(
            [
                'profile_id' => $profile->id,
            ],
            [
                'profile_id' => $profile->id,
                'email' => $args['email'],
                'email_verified' => true,
                'email_notification' => $args['email_notification'],
            ]
        );
        return $profile->settings;
    }
}
