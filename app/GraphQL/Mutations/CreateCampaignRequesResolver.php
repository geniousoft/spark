<?php

namespace App\GraphQL\Mutations;

use App\Models\Campaign;
use App\Models\CampaignRequest;
use App\Models\Profile;
use Exception;
use Illuminate\Support\Facades\DB;

final class CreateCampaignRequesResolver
{
    /**
     * @param  null  $_
     * @param  array{}  $args
     */
    public function __invoke($_, array $args)
    {

        if (!isset($args['system_wallet'])) {
            throw new Exception("Please create profile first", 1);
        }
        $systemID = $args['system_wallet'];
        $campaignCode = $args['code'];
        $profile = Profile::where('system_wallet', $systemID)->first();
        $campaign = Campaign::where('code', $campaignCode)->first();
        $this->check($args['data'], $campaign->type);

        if ($profile == null) {
            throw new Exception("Please create profile first", 1);
        }
        $kontrol = CampaignRequest::where('profile_id', $profile->id)->where('campaign_id', $campaign->id)->first();
        if ($kontrol != null) {
            throw new Exception("Allready submit a request", 1);
        }
        $project = CampaignRequest::create([
            'campaign_id' => $campaign->id,
            'data' => $args['data'],
            'payment_wallet' => $args['payment_wallet'],
            'profile_id' => $profile->id,
            'point' => 5,
            'not' => "",
            'status' => CampaignRequest::PENDING,
            'type' => $campaign->type,
            'prize' => $campaign->per_pay,
        ]);

        return $project;
    }
    public function check($jsonData, $type)
    {
        if ($type == Campaign::NANO) {
            $data = json_decode($jsonData, true);
            $subject = $data['link'];
            $pattern = '/\/status\/(\d+)/i';
            
            preg_match($pattern, $subject, $matches);
            if (isset($matches[1])) {
                try {
                    $check = DB::table('twit_links')->insert(['twit_id' => $matches[1]]);
                } catch (\Throwable $th) {
                    throw new Exception("Only one time you can send that link", 1);
                }
            } else {
                throw new Exception("its not twitter link", 1);
            }
        }
    }
}
