<?php

namespace App\GraphQL\Mutations;

use App\Models\Project;

final class CreateProjectResolver
{
    /**
     * @param  null  $_
     * @param  array{}  $args
     */
    public function __invoke($_, array $args)
    {
        $project = Project::create([
            'twitter' => $args['twitter'],
            'discord' => isset($args['discord']) ? $args['discord'] : '',
            'link' => $args['link'],
            'name' => $args['name'],
            'description' => isset($args['description']) ? $args['description'] : '',
            'logo' => $args['logo'],
            'system_wallet' => $args['system_wallet']
        ]);

        return $project;
    }
}
