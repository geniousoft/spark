<?php

namespace App\GraphQL\Mutations;

use App\Models\Campaign;
use App\Models\CampaignRequest;
use Exception;

final class DeleteCampaignResolver
{
    /**
     * @param  null  $_
     * @param  array{}  $args
     */
    public function __invoke($_, array $args)
    {


        $data =explode("SPARKDELETE",$args['val']);
        $sing = rawurldecode($data[0]);
        $wallet = rawurldecode($data[1]);
        $dwallet =  sodium_base642bin($wallet,SODIUM_BASE64_VARIANT_ORIGINAL);
        $bin = sodium_base642bin($sing,SODIUM_BASE64_VARIANT_ORIGINAL);
        $d = sodium_crypto_sign_verify_detached($bin, $args['code'],  $dwallet);
        if(!$d){
            throw new Exception("Not match sign and request", 1);
        }
        $campaign = Campaign::where('code',$args['code'])->firstOrFail();
        if($campaign->pubkey == null){
            throw new Exception("Its Already deleted", 1);
        }
        $campaign->update([
            'enabled'=>Campaign::DELETED,
            'pubkey'=>null,
        ]);
    }
}
