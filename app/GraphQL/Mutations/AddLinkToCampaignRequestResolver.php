<?php

namespace App\GraphQL\Mutations;

use App\Models\Campaign;
use App\Models\CampaignRequest;
use Exception;

final class AddLinkToCampaignRequestResolver
{
    /**
     * @param  null  $_
     * @param  array{}  $args
     */
    public function __invoke($_, array $args)
    {
        $reqID = CampaignRequest::where('id',$args['id'])->first();
        if($reqID->type != Campaign::MICRO){
            throw new Exception("Error Processing Request", 1);
        }
        $data = $reqID->data;
        $data = json_decode($data,true);
        $data['link'] = $args['link'];
        $data = json_encode($data);
        $reqID->update(['data' => $data,'status'=>CampaignRequest::WAITING]);
        
    }
}
