<?php

namespace App\GraphQL\Mutations;

use App\Models\Notification;

final class CreateNotificationResolver
{
    /**
     * @param  null  $_
     * @param  array{}  $args
     */
    public function __invoke($_, array $args)
    {
        
        $not = Notification::create([
            'not' => $args['not'],
            'is_seen' => $args['is_seen'],
            'system_wallet' => $args['system_wallet'],
        ]);

        return $not;

    }
}
