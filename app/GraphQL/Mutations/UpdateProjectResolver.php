<?php

namespace App\GraphQL\Mutations;

use App\Models\Project;

final class UpdateProjectResolver
{
    /**
     * @param  null  $_
     * @param  array{}  $args
     */
    public function __invoke($_, array $args)
    {
        $project = Project::where('id',$args['id'])->firstOrFail();
        $project->update([
            'twitter' => $args['twitter'],
            'discord' => $args['discord'],
            'link' => $args['link'],
            'name' => $args['name'],
            'description' => $args['description'],
            'logo' => $args['logo'],
            'system_wallet' => $args['system_wallet']
        ]);
        return $project;
    }
}
