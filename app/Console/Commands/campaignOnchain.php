<?php

namespace App\Console\Commands;

use App\Models\Campaign;
use App\Models\CampaignRequest;
use App\Models\CampaignStat;
use App\Models\Profile;
use App\Models\Project;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

use function PHPSTORM_META\type;

class campaignOnchain extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'py:makecampaign {campaignID}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This writes the data on chain';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $id = $this->argument('campaignID');
        $reqCampaing = CampaignRequest::where('id', $id)->firstOrFail();
      
        if($reqCampaing->status === CampaignRequest::DONE){
            return Command::SUCCESS;
        }
        $campaign = $reqCampaing->campaign;
        $profile = $reqCampaing->profile;
        $type = $campaign->type;
        $result = json_decode($campaign->data, true);
        $phrase = $result["scope"];
        $data = [
            "campaign_key" => $campaign->pubkey,
            "twitter_token" => $profile->twitter_token,
            "participant_key" => $reqCampaing->payment_wallet,
            "tweet_link" => $reqCampaing->data,
            "tweet_phrase" => $phrase
        ];

        
        switch ($campaign->type) {
            case Campaign::NANO:
                $data = base64_encode(json_encode($data));
                $resultShell = shell_exec('$(which python3) '.base_path().'/main.py ' .  Campaign::TYPE[$type] . " '" . $data . "'");
                Log::build([
                    'driver' => 'single',
                    'path' => storage_path('logs/shellLogs.log'),
                ])->info($resultShell);
                break;
            case Campaign::MICRO:
                if ($reqCampaing->status == CampaignRequest::WAITING || $reqCampaing->status == CampaignRequest::REJECTED) {
                    $cdata = json_decode($reqCampaing->data,true);
                    $data['tweet_phrase'] =  $cdata['intent'];
                    $data['tweet_link'] =  $cdata['link'];
                    $data = base64_encode(json_encode($data, JSON_UNESCAPED_SLASHES));
                    $cmd = '$(which python3) '.base_path().'/main.py ' . Campaign::TYPE[$type] . " '" . $data . "'";
                    $resultShell = shell_exec($cmd);
                } else {
                    $response = "The request has been approved or canceled.";
                    Log::build([
                        'driver' => 'single',
                        'path' => storage_path('logs/exceptionCampaignRequests.log'),
                    ])->warning($profile->twitter_handle . " tried to request a already approaved or canceled request and has received the answer: " . $response);
                }
                break;
            case Campaign::TOKEN:
                $cdata = json_decode($campaign->data,true);
                $data['helloMoonCollectionId'] = $cdata['helloMoonCollectionId'];
                $data['system_wallet'] = $reqCampaing->profile->system_wallet;
                $data = base64_encode(json_encode($data));
                $cmd = '$(which python3) '.base_path().'/main.py ' .  Campaign::TYPE[$type] . " '" . $data . "'";
                $resultShell = shell_exec($cmd);
                Log::build([
                    'driver' => 'single',
                    'path' => storage_path('logs/shellLogs.log'),
                ])->info($resultShell);
                break;
            default:
                break;
        }
        $result = json_decode($resultShell, true);
        
    
        if ($result["Status"] != false) {
            $reqCampaing->update(['status' => CampaignRequest::DONE,'not'=>$result["Message"]]);
            Log::build([
                'driver' => 'single',
                'path' => storage_path('logs/successCampaignRequests.log'),
            ])->info($result);
        } else {
            $reqCampaing->update(['status' => CampaignRequest::REJECTED,'not'=>$result["Message"]]);
            Log::build([
                'driver' => 'single',
                'path' => storage_path('logs/errorCampaignRequests.log'),
            ])->error($result);
            return "An error has occurred and logged. Please contact the administrator.";
        }

        return Command::SUCCESS;
    }
}

