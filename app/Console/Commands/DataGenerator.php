<?php

namespace App\Console\Commands;

use App\Models\Campaign;
use App\Models\CampaignRequest;
use App\Models\CampaignStat;
use App\Models\Profile;
use App\Models\Project;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

use function PHPSTORM_META\type;

class DataGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'd:g {campaignID}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This writes the data on chain';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $id = $this->argument('campaignID');
        $reqCampaing = CampaignRequest::where('id', $id)->firstOrFail();
      
        $campaign = $reqCampaing->campaign;
        $profile = $reqCampaing->profile;
        $type = $campaign->type;
        $result = json_decode($campaign->data, true);
        $phrase = $result["scope"];
        $data = [
            "campaign_key" => $campaign->pubkey,
            "twitter_token" => $profile->twitter_token,
            "participant_key" => $reqCampaing->payment_wallet,
            "tweet_link" => $reqCampaing->data,
            "tweet_phrase" => $phrase
        ];
        $data = base64_encode(json_encode($data));
        dump('python3 '.base_path().'/main.py ' .  Campaign::TYPE[$type] . " '" . $data . "'");
        return Command::SUCCESS;
    }
}

