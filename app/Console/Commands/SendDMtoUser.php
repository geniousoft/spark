<?php

namespace App\Console\Commands;

use App\Models\Profile;
use App\Notifications\TwiterApproveNotification;
use Illuminate\Console\Command;

class SendDMtoUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dm:send {profileid}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send dm to user';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {   
        $id = $this->argument('profileid');
        $profile = Profile::where('id',$id)->first();
        $msg = "Your request has approved please. Tweet the content and submit your spark to \r\n https://dashboard.sparkonline.xyz/profile";
        $profile->notify(new TwiterApproveNotification($msg));
        return Command::SUCCESS;
    }
}
