<?php

namespace App\Console\Commands;

use App\Models\NftCollection;
use Illuminate\Console\Command;

class CreateNFTCollections extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'collection:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add new NFT collection to the database';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $collectionId = $this->ask('What is the collection ID?');
        $name = $this->ask('What is the collection name?');
        $supply = $this->ask('What is the collection supply?');
        $image = $this->ask('What is the collection image?');

        NftCollection::create([
            'helloMoonCollectionId' => $collectionId,
            'name' => $name,
            'supply' => $supply,
            'image' => $image,
        ]);

        return Command::SUCCESS;
    }
}
