<?php

namespace App\Observers;

use App\Models\Campaign;
use App\Models\CampaignStat;
use App\Models\DashboardStats;
use Illuminate\Support\Facades\Artisan;

class CampaignObserver
{
    /**
     * Handle the Campaign "created" event.
     *
     * @param  \App\Models\Campaign  $campaign
     * @return void
     */
    public function created(Campaign $campaign)
    {
        $campaignId = $campaign->id; 
        DashboardStats::first()->increment('total_campaigns');
        CampaignStat::create(['campaign_id' => $campaignId, 'total_apply' => 1, 'total_pool' => 0, 'total_prize' => 0, 'average_point' => 0]);
    }

    /**
     * Handle the Campaign "updated" event.
     *
     * @param  \App\Models\Campaign  $campaign
     * @return void
     */
    public function updated(Campaign $campaign)
    {
        //
    }

    /**
     * Handle the Campaign "deleted" event.
     *
     * @param  \App\Models\Campaign  $campaign
     * @return void
     */
    public function deleted(Campaign $campaign)
    {
        //
    }

    /**
     * Handle the Campaign "restored" event.
     *
     * @param  \App\Models\Campaign  $campaign
     * @return void
     */
    public function restored(Campaign $campaign)
    {
        //
    }

    /**
     * Handle the Campaign "force deleted" event.
     *
     * @param  \App\Models\Campaign  $campaign
     * @return void
     */
    public function forceDeleted(Campaign $campaign)
    {
        //
    }
}
