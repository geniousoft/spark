<?php

namespace App\Observers;

use App\Models\Campaign;
use App\Models\CampaignRequest;
use App\Models\CampaignStat;
use App\Models\DashboardStats;
use App\Models\ProfileStat;
use Exception;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class CampaignRequestObserver
{
    /**
     * Handle the CampaignRequest "created" event.
     *
     * @param  \App\Models\CampaignRequest  $campaignRequest
     * @return void
     */
    public function created(CampaignRequest $campaignRequest)
    {
        $campaignId = $campaignRequest->campaign_id;
        $campaignStats = CampaignStat::where('campaign_id', $campaignId)->first();
        $stats = ProfileStat::where('id', $campaignRequest->profile_id)->first();


        DashboardStats::first()->increment('total_requests');

        if ($stats) {
            $stats->increment('total_apply');
            $stats->increment('total_expectant', ($campaignRequest->prize / 1000000000));
        }

        /** 
         * bir kampanyaya total basvurani kampanya istatistigi modeline yazar
         * */
        if ($campaignStats) {
            $campaignStats->increment('total_apply');
            $campaignStats->increment('average_point');
            $campaignStats->increment('total_prize', $campaignRequest->prize);
        }
        /** 
         * kampanya statuslerini update ediyor
         * */
        if ($campaignRequest->type == Campaign::NANO || $campaignRequest->type == Campaign::TOKEN) {
            Artisan::call('py:makecampaign', ['campaignID' => $campaignRequest->id]);
        }
    }

    /**
     * Handle the CampaignRequest "updated" event.
     *
     * @param  \App\Models\CampaignRequest  $campaignRequest
     * @return void
     */
    public function updated(CampaignRequest $campaignRequest)
    {
        $campaignId = $campaignRequest->id;
        if ($campaignRequest->type == Campaign::MICRO && $campaignRequest->status == CampaignRequest::WAITING) {
            //$id = '';
            //$check = DB::table('twit_links')->insert(['twit_id'=>$id]);
            $this->check($campaignRequest->data);
            Artisan::queue('py:makecampaign', ['campaignID' => $campaignId]);
        }

        if ($campaignRequest->type == Campaign::MICRO && $campaignRequest->status == CampaignRequest::APPROVED) {
            // notification
        }
        if ($campaignRequest->status == CampaignRequest::DONE) {
            $stats = ProfileStat::where('id', $campaignRequest->profile_id)->first();
            if ($stats) {
                $stats->increment('total_point');
                $stats->increment('total_worth', ($campaignRequest->prize / 1000000000));
            }
            DashboardStats::first()->increment('total_active_campaigns', ($campaignRequest->prize / 1000000000));
        }
    }

    /**
     * Handle the CampaignRequest "deleted" event.
     *
     * @param  \App\Models\CampaignRequest  $campaignRequest
     * @return void
     */
    public function deleted(CampaignRequest $campaignRequest)
    {
        //
    }

    /**
     * Handle the CampaignRequest "restored" event.
     *
     * @param  \App\Models\CampaignRequest  $campaignRequest
     * @return void
     */
    public function restored(CampaignRequest $campaignRequest)
    {
        //
    }

    /**
     * Handle the CampaignRequest "force deleted" event.
     *
     * @param  \App\Models\CampaignRequest  $campaignRequest
     * @return void
     */
    public function forceDeleted(CampaignRequest $campaignRequest)
    {
        //
    }
    public function check($jsonData)
    {
        $data = json_decode($jsonData, true);
        $subject = $data['link'];
        $pattern = '/\/status\/(\d+)/i';
        preg_match($pattern, $subject, $matches);
        if (isset($matches[1])) {
            try {
                $check = DB::table('twit_links')->insert(['twit_id' => $matches[1]]);
            } catch (\Throwable $th) {
                throw new Exception("Only one time you can send that link", 1);
            }
        } else {
            throw new Exception("its not twitter link", 1);
        }
    }
}
