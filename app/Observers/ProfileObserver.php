<?php

namespace App\Observers;

use App\Models\CampaignRequest;
use App\Models\DashboardStats;
use App\Models\Profile;
use App\Models\ProfileSettings;
use App\Models\ProfileStat;

class ProfileObserver
{
    /**
     * Handle the Profile "created" event.
     *
     * @param  \App\Models\Profile  $profile
     * @return void
     */
    public function created(Profile $profile)
    {
        ProfileStat::create(['profile_id' => $profile->id,'total_apply'=>0, 'total_point' => 0, 'total_point' => 0, 'total_worth' => 0, 'total_follower' => 0,'total_expectant' => 0]);
        ProfileSettings::create(['profile_id' => $profile->id,'email' => '','email_verified' => false,'email_notification' => true,'push_notification' => true]);
        DashboardStats::first()->increment('total_profiles');

    }

    /**
     * Handle the Profile "updated" event.
     *
     * @param  \App\Models\Profile  $profile
     * @return void
     */
    public function updated(Profile $profile)
    {
        //
    }

    /**
     * Handle the Profile "deleted" event.
     *
     * @param  \App\Models\Profile  $profile
     * @return void
     */
    public function deleted(Profile $profile)
    {
        //
    }

    /**
     * Handle the Profile "restored" event.
     *
     * @param  \App\Models\Profile  $profile
     * @return void
     */
    public function restored(Profile $profile)
    {
        //
    }

    /**
     * Handle the Profile "force deleted" event.
     *
     * @param  \App\Models\Profile  $profile
     * @return void
     */
    public function forceDeleted(Profile $profile)
    {
        //
    }
}
