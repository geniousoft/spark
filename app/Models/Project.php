<?php

namespace App\Models;

use App\Models\Scopes\OrderCreatedAtDesc;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    protected $fillable =["twitter","discord","link","name","description","logo","system_wallet"];

    public function campaigns()
    {
      return $this->hasMany(Campaign::class,'project_id','id');
    }

    
    /* ************************ ACCESSOR ************************* */
    protected $appends = ['resource_url'];


    public function getResourceUrlAttribute()
    {
        return url('/admin/projects/'.$this->getKey());
    }
    protected static function booted()
    {
        static::addGlobalScope(new OrderCreatedAtDesc);
    }
}


