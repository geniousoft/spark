<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NftCollection extends Model
{
    protected $fillable = [
        'helloMoonCollectionId',
        'image',
        'name',
        'supply',
    ];
    protected $table = 'nft_collections';

    
    
    protected $dates = [
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/nft-collections/'.$this->getKey());
    }
}
