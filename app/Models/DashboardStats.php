<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DashboardStats extends Model
{
    use HasFactory;
    protected $fillable = [
        'total_profiles',
        'total_requests',
        'total_campaigns',
        'total_active_campaigns'
    ];
}
