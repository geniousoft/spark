<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfileSettings extends Model
{
    use HasFactory;

    protected $fillable = [
        'profile_id',
        'email',
        'email_verified',
        'email_notification',
        'push_notification',
    ];

    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }



}
