<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CampaignStat extends Model
{
    use HasFactory;
    protected $fillable = [
        'total_apply',
        'total_pool',
        'total_prize',
        'average_point',
        'campaign_id'
    ];
    
    public function campaign()
    {
        return $this->belongsTo(Campaign::class,'campaign_id',"id");
    }


    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/campaing-stats/'.$this->getKey());
    }
    
}
