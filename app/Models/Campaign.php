<?php

namespace App\Models;

use App\Models\Scopes\OrderCreatedAtDesc;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    use HasFactory;

    protected $fillable = [
        'type',
        "title",
        "project_id",
        "description",
        "data",
        "per_pay",
        "code",
        "pubkey",
        "system_wallet",
        "enabled"
    ];
    const TYPE = [
        'nano',
        'micro',
        'token',
    ];

    const NANO = 0;
    const MICRO = 1;
    const TOKEN = 2;

    const STATUS = [
        'disabled',
        'enabled',
        'deleted',
    ];

    const DISABLED = 0;
    const ENABLED = 1;
    const DELETED = 2;



    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id', "id");
    }
    public function stats()
    {
        return $this->hasOne(CampaignStat::class, 'campaign_id', "id");
    }
    public function requests()
    {
        return $this->hasMany(CampaignRequest::class, 'campaign_id', "id");
    }


    /* ************************ ACCESSOR ************************* */
    protected $appends = ['resource_url'];


    public function getResourceUrlAttribute()
    {
        return url('/admin/campaigns/' . $this->getKey());
    }
    protected static function booted()
    {
        static::addGlobalScope(new OrderCreatedAtDesc);
    }
}


/*
 $table->number("type");
            $table->string("title");
            $table->foreignId("project_id");
            $table->text("description");
            $table->text("data");
            $table->number("per_pay");
            $table->number("code");
            $table->string("pubkey")->nullable();
*/