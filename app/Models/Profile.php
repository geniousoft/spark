<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Profile extends Model
{
    use Notifiable;
    use HasFactory;
    protected $fillable = ['twitter_id','twitter_avatar','twitter_handle','twitter_token','system_wallet','payment_wallet','follower'];

    protected $appends = ['resource_url'];



    public function application()
    {
        return $this->hasMany(CampaignRequest::class,"profile_id","id");
    }
    public function stats()
    {
        return $this->hasOne(ProfileStat::class,"profile_id","id");
    }

    public function settings()
    {
        return $this->hasOne(ProfileSettings::class,"profile_id","id");
    }

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/profiles/'.$this->getKey());
    }
}
