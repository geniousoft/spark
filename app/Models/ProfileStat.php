<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfileStat extends Model
{
    use HasFactory;
    protected $fillable = [
        'total_apply',
        'total_follower',
        'total_worth',
        'total_point',
        'total_expectant',
        'profile_id'
    ];


    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/profile-stats/'.$this->getKey());
    }
}

