<?php

namespace App\Models;

use App\Models\Scopes\OrderCreatedAtDesc;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class CampaignRequest extends Model
{
    use HasFactory;
    use Notifiable;


    protected $fillable = ['campaign_id', "data", "payment_wallet", "profile_id", "status", "point", "not", "type", "prize"];


    const STATUS = [
        'Pending',
        'Approved',
        'Waiting',
        'Done',
        "Rejected",
        "Error"
    ];
    const PENDING = 0;
    const APPROVED = 1;
    const WAITING = 2;
    const DONE = 3;
    const REJECTED = 4;
    const ERROR = 5;
    public function campaign()
    {
        return $this->belongsTo(Campaign::class, 'campaign_id', 'id');
    }
    public function profile()
    {
        return $this->belongsTo(Profile::class, 'profile_id', 'id');
    }

    /* ************************ ACCESSOR ************************* */
    protected $appends = ['resource_url'];

    public function getResourceUrlAttribute()
    {
        return url('/admin/campaign-requests/' . $this->getKey());
    }

   

    protected static function booted()
    {
        static::addGlobalScope(new OrderCreatedAtDesc);
    }
}
