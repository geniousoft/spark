from solders.pubkey import Pubkey
from solders.keypair import Keypair
from my_client.accounts import CampaingList
from my_client.instructions import distrubute_player
from solana.transaction import Transaction
from solana.rpc.async_api import AsyncClient
from anchorpy import Provider, Wallet
from solana.rpc.core import RPCException
from my_client.errors import from_tx_error
from dotenv import load_dotenv
import re, asyncio, sys, json, os, base64, requests, subprocess

load_dotenv()

DEVNET = "https://silent-wandering-waterfall.solana-devnet.discover.quiknode.pro/d9fe53c70fb688d6765599eb816fcac0f36da2d6/"
WEB3_CONNECTION = AsyncClient(DEVNET)

CONTROLLER_KEY = Pubkey.from_string(os.getenv("CONTROLLER_KEY"))   # bu sabit 
BOT_KEY = Keypair.from_bytes(json.loads(os.getenv("BOT_KEY"))) # bu bota ait cüzdan
HELLOMOON_API_KEY = os.getenv("HELLOMOON_API_KEY")
HELLOMOON_URL = "https://rest-api.hellomoon.io/v0/nft/collection/ownership/for-pubkey"

async def read_campain(data):
    account = await CampaingList.fetch(WEB3_CONNECTION, Pubkey.from_string(data["campaign_key"]))

    if account is None:
        print(json.dumps({"Status" : False, "Message": "PDA not found"}))
        exit()
    
    pda = account.to_json()
    pda_json = CampaingList.from_json(pda)
    return pda_json


async def distribute(data):
    campain = await read_campain(data)
    ix = distrubute_player(
        {
            "campaing_code" : campain.campaing_code 
        }, 
        {
            "controllers" : CONTROLLER_KEY,
            "owner" : campain.owner,
            "campaing" : Pubkey.from_string(data["campaign_key"]),
            "player" : Pubkey.from_string(data["participant_key"]),
            "user" : BOT_KEY.pubkey(),
        }
    )
    tx = Transaction().add(ix)
    wallet = Wallet(BOT_KEY)
    provider = Provider(WEB3_CONNECTION, wallet)
    try:
        result = await provider.send(tx, [BOT_KEY])
        return result
    except RPCException as exc:
        parsed = from_tx_error(exc)
        import traceback
        print( json.dumps( {"Status" : False, "Message": f"Transaction did not complete. Error: {str(traceback.print_exc())}, Exception: {str(parsed)}"} )) 
        exit()


def get_tweet(id):
    tweet, error = subprocess.Popen(["/home/forge/api.sparkonline.xyz/twitter_reader", str(id)], stdout=subprocess.PIPE, stderr=subprocess.STDOUT).communicate()
    return tweet.decode("utf-8")

def search_in_tweet(url, phrase):
    if "x.com" not in url:
        print(json.dumps({"Status" : False, "Message": "Not a real tweet link"}))
        exit()
    TWEET_ID = re.search('/status/(\d+)', url).group(1)
    try:
        tweet = get_tweet(TWEET_ID) 
    except:
        print(json.dumps({"Status" : False, "Message": "Cannot read the tweet"}))
        exit()
    if tweet != None and phrase in tweet:
        return True
    else:
        return False

def match_tweet(url, tweet_content):
   
    if "x.com" not in url:
        print(json.dumps({"Status" : False, "Message": "Not a real tweet link"}))
        exit()

    TWEET_ID = re.search('/status/(\d+)', url).group(1)
    try:
        tweet = get_tweet(TWEET_ID) 
        for url in tweet.entities['urls']:
            short_url = url['url']
            expanded_url = url['display_url']
            tweet.full_text = tweet.full_text.replace(short_url, expanded_url)
    except:
        print(json.dumps({"Status" : False, "Message": "Cannot read the tweet"}))
        exit()
    clean_tweet = tweet.full_text.replace('\r', '').replace('\n', '')
    clean_intent = tweet_content.replace('\r', '').replace('\n', '')
    if tweet != None and clean_tweet == clean_intent:
        return True
    else:
        False



def micro(data):
    if match_tweet(data["tweet_link"], data["tweet_phrase"]):
        return asyncio.run(distribute(data))
    else:
        print(json.dumps({"Status" : False, "Message": "tweet does not match"}))
        exit()
    

def nano(data):
    if search_in_tweet(data["tweet_link"], data["tweet_phrase"]):
       return asyncio.run(distribute(data)) 
    else:
        print(json.dumps({"Status" : False, "Message": "tweet does not contain the phrase"}))
        exit()

def token_gated(data):
    owner = data["system_wallet"]
    helloMoonCollectionId = data["helloMoonCollectionId"]
    is_owner = query_nft_ownership(owner, helloMoonCollectionId)
    if is_owner:
        return nano(data)
    else:
        print(json.dumps({"Status" : False, "Message": "this wallet doesn't have any nft from this collection"}))
        exit()

def query_nft_ownership(owner, helloMoonCollectionId):
    payload = {
        "owner": owner,
        "helloMoonCollectionId": helloMoonCollectionId
    }

    headers = {
        "accept": "application/json",
        "content-type": "application/json",
        "authorization": HELLOMOON_API_KEY 
    }
    result = requests.post(HELLOMOON_URL, json=payload, headers=headers).text
    collection = json.loads(result)["mints"]
    if len(collection) >= 1:
        return True
    else:
        return False

def main():
    #print(DATA)
    if sys.argv[1] == "micro":
        DATA = json.loads(base64.b64decode(sys.argv[2]))
        return {"Status": True, "Message": str(micro(DATA))}
    elif sys.argv[1] == "nano":
        DATA = json.loads(base64.b64decode(sys.argv[2]))
        return {"Status": True, "Message": str(nano(DATA))}
    elif sys.argv[1] == "token":
        DATA = json.loads(base64.b64decode(sys.argv[2]))
        return {"Status": True, "Message": str(token_gated(DATA))}
    else:
        return {"Status" : False, "Message": "unknown argument"} 

print(json.dumps(main()))


