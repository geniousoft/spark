import * as anchor from "@coral-xyz/anchor";
import { Program } from "@coral-xyz/anchor";
import { assert } from "chai";
import { SparkOnline } from "../target/types/spark_online";

describe("spark-online", async () => {
 
  const open = true;
  const close = true;
  anchor.setProvider(anchor.AnchorProvider.env());
  const program = anchor.workspace.SparkOnline as Program<SparkOnline>;
  console.log("Spark Online Oto Test Başlangıç");
  console.log("Spark online bir havuz sistemidir");
  console.log("Bu havuz sistemine para eklenebilir");
  console.log("Lakin para havuzdan botlar tarafından dağıtılabilir...");

  /**
   * Sadece program sahibinin çalıştırabileceği fonksiyonlar için
   */
  const programOwner = anchor.web3.Keypair.fromSecretKey(
    Buffer.from([
      127, 33, 39, 106, 220, 178, 79, 220, 62, 4, 112, 252, 168, 23, 43, 7, 217,
      139, 62, 232, 93, 225, 94, 36, 88, 95, 34, 184, 52, 52, 17, 78, 170, 248,
      138, 24, 148, 45, 98, 216, 202, 64, 31, 132, 105, 51, 222, 29, 17, 9, 73,
      17, 201, 42, 197, 31, 107, 6, 107, 31, 177, 54, 33, 147,
    ])
  );
  const programProvider = program.provider as anchor.AnchorProvider;
  const programID = anchor.workspace.SparkOnline.programId;
  const programData = new anchor.web3.PublicKey(
    "FaWpdFZwmnpWLRuCre8eeHwMCbjaYMCKbTTdh7Z2J9fN"
  );
  const name = "SuperTeamTr";
  const [campaing, bump] = await anchor.web3.PublicKey.findProgramAddressSync(
    [
      Buffer.from("campaing"),
      programOwner.publicKey.toBytes(),
      Buffer.from(name.slice(0, 32)),
    ],
    programID
  );

  const botUserWallet = await anchor.web3.Keypair.generate();

  console.log("Hazırlıklar tamam test başlıyor");

  if (open) {
    it("controller ekleme", async () => {
      console.log("Sistemdeki toplanan parayı dağıtabilmesi için bot'un cüzdan adresi ekleniyor")
      const controllers = await anchor.web3.PublicKey.findProgramAddressSync(
        [Buffer.from("campaing")],
        programID
      );
      const txHash = await program.methods
        .addController(botUserWallet.publicKey)
        .accounts({
          signer: programOwner.publicKey,
          controllers: controllers[0],
          program: programID,
          programData,
          systemProgram: anchor.web3.SystemProgram.programId,
        })
        .signers([programOwner])
        .rpc();

      await programProvider.connection.confirmTransaction(txHash);
      const allController = await program.account.controller.all();
      let account = allController[0].account.list;
      assert(
        botUserWallet.publicKey.toString() ==
          account[account.length - 1].toString()
      );
    });

    it("kampanya oluşturma", async () => {
      const txHash = await program.methods
        .campaingCreate(
          name,
          "Deneme bir kampanya",
          "https://sparkonline.xyz",
          "Deneme Kampanya başlık",
          new anchor.BN(100000000)
        )
        .accounts({
          owner: programOwner.publicKey,
          campaing,
          systemProgram: anchor.web3.SystemProgram.programId,
        })
        .signers([programOwner])
        .rpc();

      await programProvider.connection.confirmTransaction(txHash);
    });
  }
  it("hesapa para yatirma", async () => {
    await programProvider.connection.requestAirdrop(
      programOwner.publicKey,
      1 * anchor.web3.LAMPORTS_PER_SOL
    );
    const bBlance = await programProvider.connection.getBalance(campaing);
    console.log("Oluşan kampanyanın başlangıçtaki sahip olduğu SOL : ", bBlance);
    let depositePrice = anchor.web3.LAMPORTS_PER_SOL * 2;
    const txHash = await program.methods
      .deposite(name, new anchor.BN(depositePrice))
      .accounts({
        owner: programOwner.publicKey,
        user: programOwner.publicKey,
        campaing,
        systemProgram: anchor.web3.SystemProgram.programId,
      })
      .signers([programOwner])
      .rpc();
    const balance = await programProvider.connection.getBalance(campaing);

    console.log("Oluşan kampanyanın para yatırıldıkdan sonra sahip olduğu SOL : ", balance);
    assert(bBlance + depositePrice == balance);
  });

  it("para dağitma", async () => {
    console.log("Bir kampanyadan sadece controller cüzdanı ile katılımcılara para dağıtma fonksiyonu")
    const allController = await program.account.controller.all();
    let account = allController[0].publicKey;
    const kampanyaPaylaşımÜcreti = await program.account.campaingList.fetch(
      campaing
    );
    const bBlance = await programProvider.connection.getBalance(campaing);
    console.log("kampanyanın başlangıçdaki sahip olduğu SOL : ", bBlance);

    const perPay = kampanyaPaylaşımÜcreti.perPay.toNumber();

    const player = await anchor.web3.Keypair.generate();
    const txHash = await program.methods
      .distrubutePlayer(name)
      .accounts({
        controllers: account,
        player: player.publicKey,
        owner: programOwner.publicKey,
        user: botUserWallet.publicKey,
        campaing,
        systemProgram: anchor.web3.SystemProgram.programId,
      })
      .signers([botUserWallet])
      .rpc();

    const playerbalance = await programProvider.connection.getBalance(
      player.publicKey
    );
    console.log("kampanyanın da her katılımcıya dağıtılıcak SOL bedeli : ", playerbalance);
    assert(perPay == playerbalance);
    const balance = await programProvider.connection.getBalance(campaing);
    assert(bBlance - perPay == balance);
    console.log("kampanyanın dağıtım sonrası sahip olduğu SOL : ", balance);

  });

  if (close) {
    it("kampanya silme", async () => {
      const txHash = await program.methods
        .close(name)
        .accounts({
          owner: programOwner.publicKey,
          campaing,
          user: programOwner.publicKey,
        })
        .signers([programOwner])
        .rpc();

      await programProvider.connection.confirmTransaction(txHash);
    });
    it("controller çikartma", async () => {
      const controllers = await anchor.web3.PublicKey.findProgramAddressSync(
        [Buffer.from("campaing")],
        programID
      );
      const allController = await program.account.controller.all();
      let listLen = allController[0].account.list.length;

      const txHash = await program.methods
        .removeController(botUserWallet.publicKey)
        .accounts({
          signer: programOwner.publicKey,
          controllers: controllers[0],
          program: programID,
          programData,
          systemProgram: anchor.web3.SystemProgram.programId,
        })
        .signers([programOwner])
        .rpc();

      await programProvider.connection.confirmTransaction(txHash);
      const allControllerAfter = await program.account.controller.all();
      let listLenAfter = allControllerAfter[0].account.list.length;
      assert(
        listLenAfter < listLen,
        "Çıkartılan controller liste sayısı doğru"
      );
      assert(
        listLenAfter == listLen - 1,
        "Çıkartılan controller liste sayısı doğru"
      );
    });
  }
});
