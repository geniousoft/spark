use crate::program::SparkOnline;
use anchor_lang::prelude::*;
use anchor_lang::AccountsClose;
use anchor_spl::{
    associated_token::AssociatedToken,
    token::{CloseAccount, Mint, Token, TokenAccount, Transfer},
};

declare_id!("3Y5za9g2ajN97jhG3XvM2WWQ4u2gKzHNk9LXBhKQ2KCw");

#[program]
pub mod spark_online {
    use super::*;
    pub fn campaing_create(
        ctx: Context<CampaingCreate>,
        campaing_code: String,
        description: String,
        url: String,
        title: String,
        per_pay: u64,
        payment_type: bool,
    ) -> Result<()> {
        let campaing = &mut ctx.accounts.campaing;
        campaing.owner = *ctx.accounts.owner.to_account_info().key;
        campaing.description = description;
        campaing.url = url;
        campaing.title = title;
        campaing.vault = 0;
        campaing.per_pay = per_pay;
        campaing.payment_type = payment_type;
        if (payment_type) {
            campaing.token_program = ctx.accounts.token_program.key()
        }
        campaing.campaing_code = campaing_code;
        campaing.status = false;
        Ok(())
    }
    pub fn update_campaing(
        ctx: Context<CampaingUpdate>,
        _campaing_code: String,
        description: String,
        url: String,
        title: String,
    ) -> Result<()> {
        let campaing = &mut ctx.accounts.campaing;
        campaing.description = description;
        campaing.url = url;
        campaing.title = title;
        Ok(())
    }

    pub fn deposite(ctx: Context<Deposite>, _campaing_code: String, amount: u64) -> Result<()> {
        let account_lamports = **ctx.accounts.user.to_account_info().lamports.borrow();
        if account_lamports < amount {
            return Err(CampaingListError::NoMoneyInCampaing.into());
        }
        let from = ctx.accounts.user.to_account_info();
        let to = ctx.accounts.campaing.to_account_info();
        assert_eq!(ctx.accounts.campaing.payment_type , true, "Wrong type payment");
        if amount > 0 {
            let ix = anchor_lang::solana_program::system_instruction::transfer(
                &from.key(),
                &to.key(),
                amount,
            );
            let _ = anchor_lang::solana_program::program::invoke(&ix, &[from, to]);

            let campaing = &mut ctx.accounts.campaing;
            campaing.vault += amount;
            campaing.status = true;
        }

        Ok(())
    }
    pub fn deposite_spl(ctx: Context<DepositeSPL>, _campaing_code: String, amount: u64) -> Result<()> {
        let account_lamports = **ctx.accounts.user.to_account_info().lamports.borrow();
        if account_lamports < amount {
            return Err(CampaingListError::NoMoneyInCampaing.into());
        }
        assert_eq!(ctx.accounts.campaing.payment_type , false, "Wrong type payment");
        if amount > 0 {
            let (vault_pubkey, vault_bump_seed) = Pubkey::find_program_address(
                &[b"campaing", ctx.accounts.user.to_account_info().key.as_ref(), name_seed(&ctx.accounts.campaing.campaing_code)],
                &self::ID
            );
            anchor_spl::token::transfer(
                CpiContext::new(ctx.accounts.token_program.to_account_info(), anchor_spl::token::Transfer {
                    from: ctx.accounts.user.to_account_info().clone(),
                    to: ctx.accounts.campaing.to_account_info().clone(),
                    authority: ctx.accounts.user.to_account_info().clone(),
                }).with_signer(&[&[&[vault_bump_seed]]]),
                amount
            )?;

            let campaing = &mut ctx.accounts.campaing;
            campaing.vault += amount;
            campaing.status = true;
        }

        Ok(())
    }
    pub fn cancel(ctx: Context<Cancel>, _campaing_code: String) -> Result<()> {
        let campaing = &mut ctx.accounts.campaing;
        let owner = &ctx.accounts.owner;
        let user = ctx.accounts.user.to_account_info().key;
        if &campaing.owner != user {
            return Err(CampaingListError::Unauthorized.into());
        }
        if !campaing.status {
            return Err(CampaingListError::CampaingIsClose.into());
        }
        campaing.status = false;
        campaing.close(owner.to_account_info())?;
        Ok(())
    }
    pub fn close(ctx: Context<Cancel>, _campaing_code: String) -> Result<()> {
        let campaing = &mut ctx.accounts.campaing;
        let owner = &ctx.accounts.owner;
        let user = ctx.accounts.user.to_account_info().key;
        if &campaing.owner != user {
            return Err(CampaingListError::Unauthorized.into());
        }
        campaing.close(owner.to_account_info())?;
        Ok(())
    }
    /*
    Only admin account can run that function
    */
    #[access_control(Distrubute::check_signer(&ctx))]
    pub fn distrubute_player(ctx: Context<Distrubute>, _campaing_code: String) -> Result<()> {
        let account_lamports = **ctx.accounts.campaing.to_account_info().lamports.borrow();
        let amount = ctx.accounts.campaing.per_pay;
        if !ctx.accounts.campaing.status {
            return Err(CampaingListError::CampaingIsClose.into());
        }
        if account_lamports < amount {
            ctx.accounts.campaing.status = false;
            return Err(CampaingListError::InfB.into());
        }
        if amount > 0 {
            **ctx
                .accounts
                .campaing
                .to_account_info()
                .try_borrow_mut_lamports()? -= amount;
            **ctx
                .accounts
                .player
                .to_account_info()
                .try_borrow_mut_lamports()? += amount;
            ctx.accounts.campaing.vault -= amount;
        }

        Ok(())
    }
    #[access_control(DistrubuteSPL::check_signer(&ctx))]
    pub fn distrubute_player_spl(ctx: Context<DistrubuteSPL>, _campaing_code: String) -> Result<()> {
        let amount = ctx.accounts.campaing.per_pay;
        if !ctx.accounts.campaing.status {
            return Err(CampaingListError::CampaingIsClose.into());
        }
        if amount > 0 {
            let (vault_pubkey, vault_bump_seed) = Pubkey::find_program_address(
                &[b"campaing", ctx.accounts.user.to_account_info().key.as_ref(), name_seed(&ctx.accounts.campaing.campaing_code)],
                &self::ID
            );
            anchor_spl::token::transfer(
                CpiContext::new(ctx.accounts.token_program.to_account_info(), anchor_spl::token::Transfer {
                    from: ctx.accounts.campaing.to_account_info().clone(),
                    to: ctx.accounts.player.to_account_info().clone(),
                    authority: ctx.accounts.campaing.to_account_info().clone(),
                }).with_signer(&[&[&[vault_bump_seed]]]),
                amount
            )?;
            ctx.accounts.campaing.vault -= amount;
        }

        Ok(())
    }
    pub fn add_controller(ctx: Context<AdminSettings>, key: Pubkey) -> Result<()> {
        ctx.accounts.controllers.list.push(key);
        Ok(())
    }
    pub fn remove_controller(ctx: Context<AdminSettings>, key: Pubkey) -> Result<()> {
        let vec = &mut ctx.accounts.controllers.list;
        vec.remove(vec.iter().position(|x| *x == key).expect("key not found"));
        Ok(())
    }
}

fn name_seed(name: &str) -> &[u8] {
    let b = name.as_bytes();
    if b.len() > 32 {
        &b[0..32]
    } else {
        b
    }
}

#[derive(Accounts)]
#[instruction(campaing_code: String, capacity: u16)]
pub struct CampaingCreate<'info> {
    #[account(mut)]
    pub owner: Signer<'info>,
    #[account(init,
        payer=owner,
        space=CampaingList::space(&campaing_code, capacity),
        seeds=[
            b"campaing",
            owner.to_account_info().key.as_ref(),
            name_seed(&campaing_code)
        ],
        bump)]
    pub campaing: Account<'info, CampaingList>,
    pub token_program: Program<'info, Token>,
    pub system_program: Program<'info, System>,
}


#[derive(Accounts)]
#[instruction(campaing_code: String, bounty: u64)]
pub struct DepositeSPL<'info> {
    #[account(mut)]
    pub user: Signer<'info>,
    #[account(mut, has_one=owner @ CampaingListError::WrongListOwner, seeds=[b"campaing", owner.to_account_info().key.as_ref(), name_seed(&campaing_code)], bump)]
    pub campaing: Account<'info, CampaingList>,
    /// CHECK: This is not dangerous because we don't read or write from this account
    pub owner: AccountInfo<'info>,
    pub token_program : Program<'info, Token>,
    pub system_program: Program<'info, System>,
}

#[derive(Accounts)]
#[instruction(campaing_code: String, bounty: u64)]
pub struct Deposite<'info> {
    #[account(mut)]
    pub user: Signer<'info>,
    #[account(mut, has_one=owner @ CampaingListError::WrongListOwner, seeds=[b"campaing", owner.to_account_info().key.as_ref(), name_seed(&campaing_code)], bump)]
    pub campaing: Account<'info, CampaingList>,
    /// CHECK: This is not dangerous because we don't read or write from this account
    pub owner: AccountInfo<'info>,
    pub system_program: Program<'info, System>,
}

#[derive(Accounts)]
#[instruction(campaing_code: String)]
pub struct Cancel<'info> {
    #[account(mut, has_one=owner @ CampaingListError::WrongListOwner, seeds=[b"campaing", owner.to_account_info().key.as_ref(), name_seed(&campaing_code)], bump)]
    pub campaing: Account<'info, CampaingList>,
    /// CHECK: This is not dangerous because we don't read or write from this account
    pub owner: AccountInfo<'info>,
    pub user: Signer<'info>,
}
#[derive(Accounts)]
#[instruction(campaing_code: String)]
pub struct CampaingUpdate<'info> {
    #[account(mut, has_one=owner @ CampaingListError::WrongListOwner, seeds=[b"campaing", owner.to_account_info().key.as_ref(), name_seed(&campaing_code)], bump)]
    pub campaing: Account<'info, CampaingList>,
    /// CHECK: This is not dangerous because we don't read or write from this account
    pub owner: AccountInfo<'info>,
    pub user: Signer<'info>,
}

#[derive(Accounts)]
#[instruction(campaing_code: String)]
pub struct Distrubute<'info> {
    #[account(mut)]
    pub user: Signer<'info>,
    #[account(mut, has_one=owner @ CampaingListError::WrongListOwner, seeds=[b"campaing", owner.to_account_info().key.as_ref(), name_seed(&campaing_code)],bump)]
    pub campaing: Account<'info, CampaingList>,
    #[account(mut)]
    /// CHECK: This is not dangerous because we don't read or write from this account
    pub owner: AccountInfo<'info>,
    #[account(mut)]
    /// CHECK: This is not dangerous because we don't read or write from this account
    pub player: AccountInfo<'info>,
    #[account(mut)]
    pub controllers: Account<'info, Controller>,
    pub system_program: Program<'info, System>,
}

#[derive(Accounts)]
#[instruction(campaing_code: String)]
pub struct DistrubuteSPL<'info> {
    #[account(mut)]
    pub user: Signer<'info>,
    #[account(mut, has_one=owner @ CampaingListError::WrongListOwner, seeds=[b"campaing", owner.to_account_info().key.as_ref(), name_seed(&campaing_code)],bump)]
    pub campaing: Account<'info, CampaingList>,
    #[account(mut)]
    /// CHECK: This is not dangerous because we don't read or write from this account
    pub owner: AccountInfo<'info>,
    #[account(mut)]
    /// CHECK: This is not dangerous because we don't read or write from this account
    pub player: AccountInfo<'info>,
    #[account(mut)]
    pub controllers: Account<'info, Controller>,
    pub token_program : Program<'info, Token>,
    pub system_program: Program<'info, System>,
}
#[derive(Accounts)]
pub struct AdminSettings<'info> {
    #[account(mut)]
    pub signer: Signer<'info>,
    #[account(init_if_needed,payer=signer,space=Controller::space(),seeds=[b"campaing"],bump)]
    pub controllers: Account<'info, Controller>,
    #[account(constraint = program.programdata_address()? == Some(program_data.key()))]
    pub program: Program<'info, SparkOnline>,
    #[account(constraint = program_data.upgrade_authority_address == Some(signer.key()))]
    pub program_data: Account<'info, ProgramData>,
    pub system_program: Program<'info, System>,
}

#[account]
pub struct Controller {
    pub list: Vec<Pubkey>,
}

#[account]
pub struct CampaingList {
    pub description: String,
    pub token_program: Pubkey,
    pub url: String,
    pub title: String,
    pub owner: Pubkey,
    pub vault: u64,
    pub per_pay: u64,
    pub campaing_code: String,
    pub status: bool,
    pub payment_type: bool,
}

impl CampaingList {
    fn space(name: &str, capacity: u16) -> usize {
        8 + 32 + 1 + 2 + 4 + name.len() + 4 + (capacity as usize) * std::mem::size_of::<Pubkey>()
    }
}

#[error_code]
pub enum CampaingListError {
    #[msg("Specified campaing owner does not match the pubkey in the campaing")]
    WrongListOwner,
    #[msg("Unauthorized")]
    Unauthorized,
    #[msg("campaing is close")]
    CampaingIsClose,
    #[msg("BountyTooSmall")]
    BountyTooSmall,
    #[msg("No sol in left campaing")]
    NoMoneyInCampaing,
    #[msg("insufficient balance")]
    InfB,
}

impl Distrubute<'_> {
    pub fn check_signer(ctx: &Context<Distrubute>) -> Result<()> {
        let my_list = &ctx.accounts.controllers;
        let signer_pubkey = ctx.accounts.user.key();
        if !my_list.list.contains(&signer_pubkey) {
            return Err(CampaingListError::Unauthorized.into());
        }
        Ok(())
    }
}
impl DistrubuteSPL<'_> {
    pub fn check_signer(ctx: &Context<DistrubuteSPL>) -> Result<()> {
        let my_list = &ctx.accounts.controllers;
        let signer_pubkey = ctx.accounts.user.key();
        if !my_list.list.contains(&signer_pubkey) {
            return Err(CampaingListError::Unauthorized.into());
        }
        Ok(())
    }
}
impl Controller {
    fn space() -> usize {
        8 + (32 as usize) * std::mem::size_of::<Pubkey>()
    }
}



